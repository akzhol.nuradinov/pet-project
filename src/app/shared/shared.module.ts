import { CommonModule } from '@angular/common'
import { NgModule } from "@angular/core"
import { ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { NgbModule, NgbToastModule } from '@ng-bootstrap/ng-bootstrap'
import { NgOtpInputModule } from 'ng-otp-input'
import { HeaderComponent } from './components/header/header.component'
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { EmptyListComponent } from './components/empty-list/empty-list.component'
import { TranslateModule } from '@ngx-translate/core';
import { CalendarComponent } from './components/calendar/calendar.component';
import { EventCalendarComponent } from './components/calendar/event-calendar/event-calendar.component';
import { DefaultPageComponent } from './components/default-page/default-page.component'
import { FullCalendarModule } from '@fullcalendar/angular';
import { EventCalendarModelComponent } from './components/calendar/event-calendar-model/event-calendar-model.component';
import { SystemControlComponent } from './components/system-control/system-control.component';
import { PreviewComponent } from './components/preview/preview.component';
import { BypassSheetComponent } from './components/preview/bypass-sheet/bypass-sheet.component'
import { NotificationsService } from './services/notifications.service'

@NgModule({
    declarations: [
        HeaderComponent,
        SidebarComponent,
        EmptyListComponent,
        CalendarComponent,
        EventCalendarComponent,
        EventCalendarModelComponent,
        SystemControlComponent,
        PreviewComponent,
        BypassSheetComponent,
        DefaultPageComponent,
    ],
    providers: [
      NotificationsService
    ],
    exports: [
        HeaderComponent,
        SidebarComponent,
        RouterModule,
        NgOtpInputModule,
        ReactiveFormsModule,
        EmptyListComponent,
        TranslateModule,
        DefaultPageComponent,
    ],
    imports: [
        CommonModule,
        NgbModule,
        NgOtpInputModule,
        ReactiveFormsModule,
        RouterModule,
        TranslateModule,
        FullCalendarModule
    ]
})

export class SharedModule { }
