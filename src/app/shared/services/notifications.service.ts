import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';

@Injectable()
export class NotificationsService {
  constructor(
    private toastrService: ToastrService,
    private translateService: TranslateService
  ) { }

  reportError(err: any) {
    this.toastrService.error(this.getErrorMessage(err));
  }

  private getErrorMessage(err: any): string {
    if (err instanceof HttpErrorResponse) {
      if (err.status === 0) {
        return this.translateService.instant("shared.cannotConnectToServer");
      } else {
        return err.error?.title || this.translateService.instant("shared.unknownError");
      }
    }

    return (err as any)?.error?.title || this.translateService.instant("shared.unknownError");
  }
}
