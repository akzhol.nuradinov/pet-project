import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Observable } from 'rxjs'
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
    private router: Router) { }

  get token(): string | null {
    return sessionStorage.getItem('token') as string
  }

  get id(): string | null {
    return sessionStorage.getItem('id') as string;
  }

  get role(): string | null {
    return sessionStorage.getItem('role') as string
  }

  // Name of the user
  get getLogin(): string | null {
    return sessionStorage.getItem('login') as string
  }

  get legalStatus(): string | null {
    return sessionStorage.getItem('legalStatus') as string
  }

  get isLogged(): boolean {
    return !!this.token
  }
  // ДРИПиУа
  get isEmployee(): boolean {
    return this.role === 'ROLE_CREDIT_MANAGER'
  }

  // Главный ДРИПиУА
  get isTopEmployee(): boolean {
    return this.role === 'ROLE_TOP_CREDIT_MANAGER'
  }

    // Директор ДРИПиУА
    get isBoardEmployee(): boolean {
      return this.role === 'ROLE_DIRECTOR_CREDIT_MANAGER'
    }

  // Сотрудник юридической службы
  get isLegalService(): boolean {
    return this.role === 'ROLE_LEGAL_SERVICE'
  }

  // Директор юридической службы
  get isTopLegalService(): boolean {
    return this.role === 'ROLE_TOP_LEGAL_SERVICE'
  }

  // Сотрудник залоговой службы
  get isCollateralService(): boolean {
    return this.role === 'ROLE_COLLATERAL_SERVICE'
  }

  // Директор залоговой службы
  get isTopCollateralService(): boolean {
    return this.role === 'ROLE_TOP_COLLATERAL_SERVICE'
  }

  // Сотрудник службы риск-менеджмента
  get isRiskManagerService(): boolean {
    return this.role === 'ROLE_RISK_MANAGEMENT_SERVICE'
  }

  // Директор службы риск-менеджмента
  get isTopRiskManagerService(): boolean {
    return this.role === 'ROLE_TOP_RISK_MANAGEMENT_SERVICE'
  }

  // Секретарь
  get isSecretary(): boolean {
    return this.role === 'ROLE_SECRETARY'
  }

  //Инвестиционный комитет
  get isInvestCommitte(): boolean {
    return this.role === 'ROLE_INVEST_COMMITTEE'
  }

  // Клиент
  get isClient(): boolean {
    return this.role === 'ROLE_CLIENT'
  }
  // Админ
  get isAdmin(): boolean {
    return this.role === 'ROLE_ADMIN'
  }

  // Председатель
  get isChairman(): boolean {
    return this.role === 'ROLE_BOARD_CHAIRMAN'
  }

  // Заместитель Председателя
  get isDeputyChairman(): boolean {
    return this.role === 'ROLE_DEPUTY_BOARD_CHAIRMAN'
  }

  // Кадровая служба
  get isHumanResource(): boolean {
    return this.role === 'ROLE_HUMAN_RESOURCES'
  }
  // Экономическая служба
  get isEconomicalService(): boolean {
    return this.role === 'ROLE_ECONOMICAL_SERVICE'
  }
  // Директор экономической службы
  get isTopEconomicalService(): boolean {
    return this.role === 'ROLE_TOP_ECONOMICAL_SERVICE'
  }

  // Канцелярия
  get isChancellery(): boolean {
    return this.role === 'ROLE_CHANCELLERY'
  }

  changeLanguage(value: string): Observable<any> {
    return this.http.get(`/lang/${value}`)
  }

  login(data: { iin?: string, phone?: number, password?: string }): Observable<any> {
    return this.http.post(`${environment.api}/check`, data)
  }

  logout(): void {
    sessionStorage.clear()
    this.router.navigateByUrl('/')
  }

  authenty(data: { phone?: number }): Observable<any> {
    return this.http.post(`${environment.api}/send-activation`, data)
  }

  activate(data: string): Observable<any> {
    const formData = { key: data }
    return this.http.post(`${environment.api}/activate`, formData)
  }

  passwordSend(data: any): Observable<any> {
    return this.http.post(`${environment.api}/account/reset-password`, data)
  }

  authenticate(data): Observable<any> {
    return this.http.post(`${environment.api}/authenticate`, data)
  }

}
