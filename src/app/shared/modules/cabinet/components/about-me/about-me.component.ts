import { Component, OnInit } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { IKeyEds } from 'src/app/models/model.model'
import { IUserEMPloyee } from 'src/app/models/user.model'
import { CabinetService } from 'src/app/services/cabinet.service'
import { AddEdsModalComponent } from '../add-eds-modal/add-eds-modal.component'

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit {

  keyList: IKeyEds[]
  user: IUserEMPloyee

  constructor(
    private cabinetService: CabinetService,
    private toastrService: ToastrService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getEdsKey()
    this.cabinetService.getUser().subscribe(res => this.user = res)
  }

  getEdsKey(): void {
    this.cabinetService.getEdsKey().subscribe(res => this.keyList = res)
  }

  showAddEdsModal(): void {
    const modal = this.modalService.open(AddEdsModalComponent, { backdrop: 'static', keyboard: true })
    modal.closed.subscribe(res => { if (res) this.getEdsKey() })
  }

  deleteKey(key: IKeyEds): void {
    this.cabinetService.deleteEdsKey(key.id).subscribe(() => this.getEdsKey())
  }
}
