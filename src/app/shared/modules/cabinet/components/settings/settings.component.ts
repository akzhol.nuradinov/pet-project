import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { CabinetService } from 'src/app/services/cabinet.service'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  formChangePass: FormGroup
  constructor(private formBuilder: FormBuilder, private cabinetService: CabinetService) { }

  ngOnInit(): void {
    this.initForms()
  }

  initForms(): void {
    this.formChangePass = this.formBuilder.group({
      password: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(32)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(32)]]
    })
  }

  passChange(): void {
    console.log('formChangePass: ', this.formChangePass.value)
    this.cabinetService.passwordChange(this.formChangePass.value).subscribe(res => {
      console.log('passChange res: ', res)
    })
  }
}
