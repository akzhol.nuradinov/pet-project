import { Component } from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { CabinetService } from 'src/app/services/cabinet.service'

@Component({
  selector: 'app-add-eds-modal',
  templateUrl: './add-eds-modal.component.html',
  styleUrls: ['./add-eds-modal.component.scss']
})
export class AddEdsModalComponent {

  files: File[]
  constructor(
    public activeModal: NgbActiveModal,
    private cabinetService: CabinetService
  ) { }

  fileOnChange($event): void {
    if ($event.target.files.length > 0) this.files = Object.values($event.target.files)
  }

  sendKey(): void {
    this.cabinetService.sendEdsKey(this.files).subscribe(res => {
      this.activeModal.close(true)
    })
  }
}
