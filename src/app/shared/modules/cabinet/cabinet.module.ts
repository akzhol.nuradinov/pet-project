import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { SharedModule } from '../../shared.module'
import { AboutMeComponent } from './components/about-me/about-me.component'
import { AddEdsModalComponent } from './components/add-eds-modal/add-eds-modal.component'
import { SettingsComponent } from './components/settings/settings.component'
import { DefaultPageComponent } from './default-page/default-page.component'

const routes: Routes = [
  {
    path: '', component: DefaultPageComponent, children: [
      { path: 'about-me', component: AboutMeComponent },
      { path: 'settings', component: SettingsComponent }
    ]
  }
]

@NgModule({
  declarations: [
    AboutMeComponent,
    SettingsComponent,
    DefaultPageComponent,
    AddEdsModalComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class CabinetModule { }
