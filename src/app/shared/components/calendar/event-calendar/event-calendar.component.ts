import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import ruLocale from '@fullcalendar/core/locales/ru';
import listPlugin from '@fullcalendar/list';
import timeGridPlugin from '@fullcalendar/timegrid'
import bootstrap5Plugin from '@fullcalendar/bootstrap5';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EventCalendarModelComponent } from '../event-calendar-model/event-calendar-model.component';
import { EmployeeService } from 'src/app/services/employee.service';
import { FullCalendarComponent } from '@fullcalendar/angular';

@Component({
  selector: 'app-event-calendar',
  templateUrl: './event-calendar.component.html',
  styleUrls: ['./event-calendar.component.scss']
})
export class EventCalendarComponent implements OnInit,  AfterViewInit {
  form: FormGroup;
  events = [];
  calendarOptions: CalendarOptions;

  @ViewChild('calendar') calendarComponent: FullCalendarComponent; 
  private calendarApi

ngAfterViewInit(){
  this.calendarApi = this.calendarComponent.getApi();
  let activeStart = this.calendarApi.view.activeStart.toISOString();
  let activeEnd = this.calendarApi.view.activeEnd.toISOString();

  this.form = this.fb.group({
    start: activeStart,
    end: activeEnd,
  });

  this.getEvents();
}

ngOnInit(): void {
  this.calendarInit()
}

calendarInit() {
 this.calendarOptions = {
    plugins: [dayGridPlugin, interactionPlugin, listPlugin, timeGridPlugin, bootstrap5Plugin ],
    initialView: 'dayGridMonth',
    timeZone: 'UTC',
    eventTimeFormat: {
      hour: 'numeric',
      minute: '2-digit',
      meridiem: false
    },
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    },
    slotLabelFormat: {
      hour: 'numeric',
      minute: '2-digit',
      omitZeroMinute: false,
    },
    businessHours: true,
    themeSystem: "bootstrap5",
    selectable: true,
    select: this.selectDay.bind(this),
    locale: ruLocale,
    events: this.events,
    eventClick: this.selectEvent.bind(this),
  }
}


  constructor(private modalService: NgbModal,
    private fb: FormBuilder,
    private employeeService: EmployeeService) { }
  
  getEvents() {
    this.employeeService.calendarEvents(this.form.value).subscribe(data => {
    this.calendarOptions.events = data
  });
  
  }

  handler(date) {
    if(date.start >= Date.now()) {
      let btn = document.getElementById('btn');
      btn.click();
    }
  }

  selectEvent(date?: any) {
    const ref = this.modalService.open(EventCalendarModelComponent, {size: 'md'})
    if (!!date) {
    let id = date?.event?._def?.publicId;
    let array= this.calendarOptions.events as Array<any>
    ref.componentInstance.eventId = date?.event?._def?.publicId;
    ref.componentInstance.event = array.find(i => i.id == id) 
    }

  }

  selectDay(date: any) {
    const ref = this.modalService.open(EventCalendarModelComponent, {size: 'md'})
    ref.componentInstance.startDate = date?.start;
    ref.componentInstance.endDate = date?.end;
  }



  }

