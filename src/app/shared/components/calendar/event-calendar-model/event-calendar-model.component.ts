import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-event-calendar-model',
  templateUrl: './event-calendar-model.component.html',
  styleUrls: ['./event-calendar-model.component.scss']
})
export class EventCalendarModelComponent implements OnInit {
  form: FormGroup;
  events = []
  startDate: any;
  endDate: any;
  eventId: string;
  event: any;

  constructor(private fb: FormBuilder,
    private toastrService: ToastrService,
    private employeeService: EmployeeService,
    private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.init()
    if (this.startDate && this.endDate) {
      this.form.patchValue({
        title: '',
        start: this.startDate.toISOString().slice(0, 16),
        end: this.endDate.toISOString().slice(0, 16),
      })
    }
    
    if (!!this.event) {
      this.form.patchValue({
              title: this.event.title,
              start: this.event.start.slice(0,21),
              end: this.event.end.slice(0,21),
              color: this.event.color
            })
    }
  }

  init() {
    this.form = this.fb.group({
      id: this.eventId,
      title: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required],
      color: ['#563d7c'],
      allDay: [''],
      commment: ['']
    });    
  }

  submit() {
    this.employeeService.addCalendarEvent(this.form.value).subscribe(() => {
      this.toastrService.success('Успешно добавлен')
      window.location.reload();
    })
  }

  update() {
    this.employeeService.updateCalendarEvent(this.form.value).subscribe(() => {
      this.toastrService.success('Успешно добавлен')
      window.location.reload();
    })
  }

  removeEvent() {
    this.employeeService.removeCalendarEvent(this.eventId).subscribe(data => {
    this.toastrService.success('Удален')
    window.location.reload();
  }) }

  close() {
    this.activeModal.close()
  }

}
