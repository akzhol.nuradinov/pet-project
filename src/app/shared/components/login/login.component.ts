import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'
import { Router } from '@angular/router'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { cleanNullProps, passwordMatch } from 'src/app/utils'
import { AuthService } from '../../services/auth.service'
import { NotificationsService } from '../../services/notifications.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  status: string
  key: string
  loading: boolean
  form: FormGroup<{
    phone: FormControl<number>;
    newPassword: FormControl<string>;
    confirmPassword: FormControl<string>;
    loginGroup: FormGroup;
    key: FormControl<any>;
}>
  public config = {
    length: 6,
    inputClass: 'digit-otp',
    containerClass: 'd-flex justify-content-around',
    passwordMinLength: 4,
    passwordMaxLength: 32,
  };

  readonly routesMap = {
    ROLE_CLIENT: ['/customer/my-applications'],
    ROLE_ADMIN: ['/admin/employee'],
    ROLE_CREDIT_MANAGER: ['/employee'],
    ROLE_DIRECTOR_CREDIT_MANAGER: ['/employee'],
    ROLE_TOP_CREDIT_MANAGER: ['/employee'],
    ROLE_TOP_LEGAL_SERVICE: ['/employee'],
    ROLE_LEGAL_SERVICE: ['/employee'],
    ROLE_TOP_ECONOMICAL_SERVICE: ['/employee'],
    ROLE_ECONOMICAL_SERVICE: ['/employee'],
    ROLE_RISK_MANAGEMENT_SERVICE: ['/employee'],
    ROLE_TOP_RISK_MANAGEMENT_SERVICE: ['/employee'],
    ROLE_SECRETARY: ['/employee'],
    ROLE_BOARD_CHAIRMAN: ['/employee'],
    ROLE_DEPUTY_BOARD_CHAIRMAN: ['/employee'],
    ROLE_BOARD_DIRECTORS: ['/employee'],
    ROLE_GOVERNING_BODY: ['/employee'],
    ROLE_CHANCELLERY: ['/employee'],
    ROLE_HUMAN_RESOURCES: ['/human-resource/candidates'],
    ROLE_STATE_PURCHASER: ['/public-procurement']
  }
  notifications: any
  openEye: string = 'close';

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private AuthService: AuthService,
    public modalService: NgbModal,
    private toastrService: ToastrService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    if (sessionStorage.getItem('token')) {
      this.router.navigate(this.routesMap[sessionStorage.getItem('role')])
    }

    this.form = this.fb.group({
      phone: [+7, [Validators.required, Validators.minLength(11), Validators.maxLength(10)]],
      newPassword: ['', [Validators.required, Validators.minLength(this.config.passwordMinLength), Validators.maxLength(this.config.passwordMaxLength), Validators.pattern('(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{'+ this.config.passwordMinLength + ',}')]],
      confirmPassword: ['', [Validators.required, Validators.minLength(this.config.passwordMinLength), Validators.maxLength(this.config.passwordMaxLength)]],
      key: null,
      loginGroup: this.fb.group({
        iin: [null, [Validators.required, Validators.minLength(4)/* This is for enabling test accounter user/admin */ /*Validators.minLength(12)*/, Validators.maxLength(12)]],
        password: [null, [Validators.required]],
      })
    })
  }

  validation() {
    let letter = document.getElementById("letter");
    let capital = document.getElementById("capital");
    let number = document.getElementById("number");
    let length = document.getElementById("length");
    if (letter && capital && number && length) {
          // Validate lowercase letters
      var lowerCaseLetters = /[a-z]/g;
      if(this.form.get('newPassword').value.match(lowerCaseLetters)) {
        letter.classList.remove("invalid");
        letter.classList.add("valid");
      } else {
        letter.classList.remove("valid");
        letter.classList.add("invalid");
      }

        // Validate capital letters
      var upperCaseLetters = /[A-Z]/g;
      if(this.form.get('newPassword').value.match(upperCaseLetters)) {
        capital.classList.remove("invalid");
        capital.classList.add("valid");
      } else {
        capital.classList.remove("valid");
        capital.classList.add("invalid");
      }

        // Validate numbers
      var numbers = /[0-9]/g;
      if(this.form.get('newPassword').value.match(numbers)) {
        number?.classList.remove("invalid");
        number?.classList.add("valid");
      } else {
        number.classList.remove("valid");
        number.classList.add("invalid");
      }

      // Validate length
      if(this.form.get('newPassword').value.length >= this.config.passwordMinLength) {
        length?.classList.remove("invalid");
        length?.classList.add("valid");
      } else {
        length.classList.remove("valid");
        length.classList.add("invalid");
      }
    }

  }

  vision() {
    const inputPath = document.getElementById('password') as HTMLInputElement;
    
    if(inputPath.type === 'password') {
      inputPath.type = 'text';
      this.openEye = 'open';
    } else {
      inputPath.type = 'password';
      this.openEye = 'close';
    }
  }

  onConfirm() {
    return (passwordMatch(
      this.form.get('password').value,
      this.form.get('confirmPassword').value
    ))()
  }

  sendIIN() {
    this.AuthService.login(cleanNullProps(this.form.value)).subscribe(
      (data) => {
        this.status = data.status
      },
      (err) => {
        this.notificationsService.reportError(err);
        this.status = err?.error?.title
      }
    )
  }

  sendPhone(twoDauthenty) {
    this.AuthService.authenty(cleanNullProps(this.form.value)).subscribe((data) => {
      this.modalService.open(twoDauthenty, {
        windowClass: 'modal-holder',
        backdrop: 'static',
      })
    }, error => {
      this.notificationsService.reportError(error);
    })
  }

  createNewPassword() {
    this.form.patchValue({ key: this.key })
    this.AuthService.passwordSend(cleanNullProps(this.form.value)).subscribe((data) => {
      this.toastrService.success(data?.message || 'Сохранено')
      this.router.navigateByUrl('/')
    }, err => {
      this.notificationsService.reportError(err);
    })
    this.router.navigateByUrl('/')
  }

  authenticate() {
    if (!this.form.controls.loginGroup.valid) {
      return;
    }

    this.form.disable();
    this.loading = true;
    const authRequest = cleanNullProps(this.form.controls.loginGroup.value);
    this.AuthService.authenticate(authRequest).subscribe((data) => {
      sessionStorage.setItem('id', data.id)
      sessionStorage.setItem('login', data.firstName + ' ' + data.lastName)
      sessionStorage.setItem('token', data.id_token)
      sessionStorage.setItem('role', data.role)
      if (typeof data.legalStatus !== 'undefined') {
        sessionStorage.setItem('legalStatus', data.legalStatus)
      }
      this.router.navigate(this.routesMap[data.role])
      this.form.enable();
      this.loading = false;
    }, (err) => {
      this.form.enable();
      this.notificationsService.reportError(err);
      this.loading = false;
    })
  }

  onOtpChange(event: string) {
    if (event.length === 6) {
      this.key = event
      this.AuthService.activate(event).subscribe(() => {
        this.status = 'set password'
        this.modalService.dismissAll()
      })
    }
  }
}
