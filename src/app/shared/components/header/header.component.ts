import { Component, OnInit } from '@angular/core'
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap'
import { AdminService } from 'src/app/services/admin.service'
import { EmployeeService } from 'src/app/services/employee.service'
import { AuthService } from './../../services/auth.service'
import { ToastrService } from 'ngx-toastr'
import { TranslateService } from '@ngx-translate/core'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  user: string
  notifications: any
  lang;
  constructor(public authService: AuthService,
              private employeeService: EmployeeService,
              private translateService: TranslateService,
              private toastrService: ToastrService,
              private adminService: AdminService,
              public config: NgbDropdownConfig) {
    config.placement = 'bottom-end'
  }



  ngOnInit() {
    this.lang = sessionStorage.getItem('lang') || this.translateService.currentLang;
    this.user = this.authService.getLogin
    this.init();
    
  }

  init() {
    this.adminService.notifications().subscribe(data => {
      this.notifications = data
    })
  }

  deleteNotification(id: number): void {
    this.adminService.deleteNotification(id).subscribe(() =>
      this.init())
  }

  changeLan(event): void {
    this.authService.changeLanguage(event.target.value).subscribe(() => {
      sessionStorage.setItem('lang', event.target.value);
      window.location.reload();
    }, err => {
      this.toastrService.error('Ошибка')
    })
  }

  search(event): void {
    this.employeeService.subject.next(event.target.value);
    // console.log(event.target.value)
  }

}