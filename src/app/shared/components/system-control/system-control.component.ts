import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-system-control',
  templateUrl: './system-control.component.html',
  styleUrls: ['./system-control.component.scss']
})
export class SystemControlComponent implements OnInit {
form: FormGroup;
userList: UserList
  constructor(private fb: FormBuilder,
    private router: Router,
    private adminService: AdminService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
          page: 0,
          size: 10,
          from: '',
          to: ''
        });

    this.getUsers();
  }


  getUsers() {
    this.adminService.systemStatus(cleanNullProps(this.form.value)).subscribe((status) => {
      this.userList = status;
    })
  }

  sort(value: string): void {}

  setPage(event) {
    this.form.patchValue({
      page: event - 1,
    })
    this.getUsers()
  }

  submit() {
    this.getUsers()
  }

}
