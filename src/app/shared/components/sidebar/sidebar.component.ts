import { Component, HostListener, OnInit, ViewEncapsulation } from '@angular/core'
import { Router } from '@angular/router'
import { AuthService } from './../../services/auth.service'

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  public paddingTop = '144px';
  public collapsed = false;
  activeIdk = ''
  constructor(
    public authService: AuthService,
    private router: Router) {
   }
  ngOnInit(): void {
  this.activeIdk = this.router.url.split('/')[1];
  }


  isCabinet(): boolean {
    return this.router.url.split('/').indexOf('cabinet') > 0 ? true : false
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    if (window.pageYOffset > 96) {
      this.paddingTop = '48px'
    } else {
      this.paddingTop = '144px'
    }
  }
}
