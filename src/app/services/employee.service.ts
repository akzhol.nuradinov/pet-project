import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  sendID = new Subject<number>();
  subject = new BehaviorSubject<null>(null);
  techSupportRequestCreated = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  getUsers(filter: { page?: number, size?: number, region?: number , search?: string}): Observable<any> {
    return this.http.get(`/applications/lease`, { params: filter })
  }

  detail(id: number): Observable<any> {
    return this.http.get(`/applications/${id}`)
  }

  acceptApplication(filter: { status: number, id: number }): Observable<any> {
    return this.http.post(`/applications/${filter.id}/status?id=${filter.status}`, { params: filter })
  }

  verifyDocument(data: { userId?: number, id: number }): Observable<any> {
    return this.http.post(`/documents/${data.id}/verify`, data.id)
  }

  revisionDocument(data: { userId?: number, id: number, comments: string }): Observable<any> {
    return this.http.post(`/documents/${data.id}/revision`, { comments: data.comments })
  }

  refixDocument(data: {id: number, document: any }): Observable<any> {
    console.log(data)
    const formData = new FormData();
    formData.append('document', data.document[0]);
    return this.http.post(`/documents/${data.id}/revision/submit`, formData)
  }

  getEmployeesList(filter?: { applicationId?: number }): Observable<any> {
    return this.http.get(`/users`, {params: {applicationId: filter.applicationId}})
  }

  downloadDocument(id: number): Observable<any> {
    return this.http.get(`/documents/${id}`, { responseType: 'blob' })
  }

  getHistorList(id: number): Observable<any> {
    return this.http.get(`/applications/${id}/history`)
  }

  myConclusions(id: number, filter?: { mine: boolean }): Observable<any> {
    return this.http.get(`/${id}/document-conclusions`, { params: filter })
  }

  sendTo(id: number): Observable<any> {
    return this.http.get(`/dict-service-groups`, { params: { applicationId: id } })
  }

  sendApplication(form): Observable<any> {
    return this.http.post(`/application-access`, form)
  }

  submitConclusion(form): Observable<any> {
    let formData = new FormData
    for (let i in form) {
      if (typeof form[i] === 'object' && i === 'files' || i === 'documents') {
        form[i].forEach(file => formData.append('file', file))
      } else {
        formData.append(i, form[i])
      }

    }

    return this.http.post(`/document-conclusions`, formData)
  }

  updateConclusion(form, id: number): Observable<any> {
    let formData = new FormData();
    for (let i in form) {
      formData.append(i, form[i]);
    }
    return this.http.put(`/document-conclusions/${id}`, formData)
  }

  applicationProgress(id: number): Observable<any> {
    return this.http.get(`/applications/${id}/process`)
  }

  assignNumberOfApplication(data: { id: number, number: number }): Observable<any> {
    return this.http.post(`/applications/${data.id}/number`, { number: data.number })
  }

  executerTask(receiver: { receiverLogin: number }, id: number): Observable<any> {
    return this.http.post(`/application/${id}/assign`, receiver)
  }

  dictType(): Observable<any> {
    return this.http.get(`/dict-tech-support-type`)
  }

  repair(id: number): Observable<any> {
    return this.http.post(`/document-conclusions/${id}/status?id=2`, {})
  }

  techSupport(data: { message: string, typeId: string, documents: Array<File> }): Observable<any> {
    const formData = new FormData
    formData.append('message', data.message)
    formData.append('typeId', data.typeId)
    for (const key of data.documents) {
      formData.append('file', key)
    }
    return this.http.post(`/technical-support`, formData)
  }

  onForm(): Observable<any> {
    return this.http.get(`/test`)
  }

  regionsResource(): Observable<any> {
    return this.http.get(`/dict-regions`)
  }
  statusesResource(): Observable<any> {
    return this.http.get(`/dict-application-statuses/leasing`)
  }
  changeStatus(status: { id: number, name: string, nameKz: string }): Observable<any> {
    return this.http.post(`/dict-application-statuses`, status)
  }

  sendDocuments(data, id: number): Observable<any> {
    const formData = new FormData();
    for (const key in data) {
      formData.append(key, data[key]);
     }
    return this.http.post(`/applications/candidates/${id}/document`, formData)
  }

  historyDocument(id: number): Observable<any> {
    return this.http.get(`/documents/${id}/history`)
   }

   calendarEvents(filter: {start: string, end: string}): Observable<any> {
    return this.http.get(`/calendar/events`, {params: filter})
   }
   addCalendarEvent(event): Observable<any> {
    return this.http.post(`/calendar/events`, event)
   }

   updateCalendarEvent(event): Observable<any> {
    return this.http.put(`/calendar/events/${event.id}`, event)
   }

   removeCalendarEvent(id): Observable<any> {
    return this.http.delete(`/calendar/events/${id}`)
   }
}
