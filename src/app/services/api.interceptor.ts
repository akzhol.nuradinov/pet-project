import {
  HttpErrorResponse,
  HttpEvent, HttpHandler, HttpInterceptor, HttpRequest
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Observable, catchError, throwError } from 'rxjs'
import { environment } from './../../environments/environment'
import { AuthService } from '../shared/services/auth.service'

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor(
    private router: Router,
    private toastrService: ToastrService,
    private authService: AuthService
  ) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const token: string = sessionStorage.getItem('token')
    let authHeader = {}
    if (token) {
      authHeader = { Authorization: `Bearer ${token}` }
    }

    if(!request.url.includes('/assets/i18n/')) {
    const modified = request.clone({
      url: (!request.url.startsWith(environment.api) ? environment.api : '') + request.url,
      setHeaders: {
        ...authHeader
      }
    })

      return next.handle(modified).pipe(catchError((err: HttpErrorResponse) => {
        switch (err.status) {
          case 400:
            this.toastrService.error(err.error.title)
            break
          case 401:
            this.authService.logout();
            break
          case 403:
            this.authService.logout();
            break
          case 500:
            const m = err?.error?.message ? err?.error?.message : err.message
            this.toastrService.error(err?.error?.detail, m)
            break
        }
        return throwError(() => err)
      }))
    }  else {
      return next.handle(request)
    }


    
  }
}
