import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  constructor(private http: HttpClient) { }


  getUsers(filter: { id?: number, page?: number, size?: number, search?: string }): Observable<any> {
    return this.http.get('/applications/lease', { params: filter })
  }

  getActivityTypes(): Observable<any> {
    return this.http.get('/dict-activity-types')
  }

  request(data): Observable<any> {
    const formData =  new FormData();
   for(const i in data) {
    if(typeof data[i] === 'object') {
      for(const j in data[i]) {
        formData.append(j, data[i][j])
      }
    }
    
   }
    return this.http.post(`/applications/lease`, formData)
   }

}
