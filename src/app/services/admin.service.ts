import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable, Subject } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  addedUSER = new Subject<boolean>();

  constructor(private http: HttpClient) { }


  getEmployees(filter?: {page?: number, size?: number, search?: string}): Observable<any> {
    return this.http.get(`/users/employees`, { params: filter })
  }

  getPersonalDataEmployees(id: number): Observable<any> {
    return this.http.get(`/users/employees/${id}`)
  }

  getClients(filter: {page?: number, size?: number, search?: string}): Observable<any> {
    return this.http.get(`/users/clients`, { params: filter })
  }

  getPersonalDataClient(id: number): Observable<any> {
    return this.http.get(`/users/clients/${id}`)
   }

  getRoles(): Observable<any> {
    return this.http.get(`/authorities`)
  }
  createUser(data): Observable<any> {
    return this.http.post(`/users`, data)
  }

  updateUser(data): Observable<any> {
    return this.http.put(`/users`, data)
  }

  getAdmins(): Observable<any> {
    return this.http.get(`/users/admins`)
  }

  updateUSer(data): Observable<any> {
    return this.http.put(`/users`, { params: data })
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`/users/${id}`)
  }
  notifications(): Observable<any> {
    return this.http.get(`/notification`)
  }
  deleteNotification(id: number): Observable<any> {
    return this.http.get(`/notification/${id}/read`)
  }

  technicalSupport(filter: {page: number, size: number}): Observable<any> {
    return this.http.get(`/technical-support`, {params: filter})
  }
  technicalSupportDetail(id: number): Observable<any> {
    return this.http.get(`/technical-support/${id}`);
  }

  technicalSupportStatus(): Observable<any> {
    return this.http.get(`/dict-tech-support-status`)
  }

  sortByPosition(): Observable<any> {
    return this.http.get(`/dict-service-groups`)
  }

  getServiceGroup(id: string): Observable<any> {
   return this.http.get(`/dict-positions?serviceGroupId=${id}`)
  }

  assginAdminTechSup(login: string, id: number): Observable<any> {
    return this.http.post(`/technical-support/${id}/assign`, {login: login})
  }

  assginStatusTechSup(taskId: string, id: number): Observable<any> {
    return this.http.post(`/technical-support/${id}/status`, {statusId: taskId});
  }

  assignNumberTech(id: number, number: number): Observable<any> {
  return this.http.post(`/technical-support/${id}/number`, {number: number});
  }

  systemStatus(filter?: any): Observable<any> {
    return this.http.get(`/admin/security`, {params: filter})
  }

}
