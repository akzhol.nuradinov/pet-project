import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HumanResourcesService {
  candidateAdded = new Subject<boolean>();

  constructor(private http: HttpClient) { }

  getCandidates(data?: {page: number, size: number, approved?: boolean}): Observable<any> {
    return this.http.get(`/applications/candidates`, {params: data})
  }

  getDismissal(data?: {page: number, size: number, approved?: boolean}): Observable<any> {
    return this.http.get(`/applications/dismissal`, {params: data})
  }

  getPromotion(data?: {page: number, size: number, approved?: boolean}): Observable<any> {
    return this.http.get(`/applications/promotion`, {params: data})
  }

  getAbsence(data?: {page: number, size: number, approved?: boolean}): Observable<any> {
    return this.http.get(`/applications/absence`, {params: data})
  }

  getPersonalDataCandidates(id: number): Observable<any> {
    return this.http.get(`/applications/candidates/${id}`)
  }

  getPersonalDataDismissal(id: number): Observable<any> {
    return this.http.get(`/applications/dismissal/${id}`)
  }

  getPersonalDataPromotion(id: number): Observable<any> {
    return this.http.get(`/applications/promotion/${id}`)
  }

  getPersonalDataVacation(id: number): Observable<any> {
    return this.http.get(`/applications/vacations/${id}`)
  }

  getPersonalBusinessTrip(id: number): Observable<any> {
    return this.http.get(`/applications/business-trip/${id}`)
  }

  getVacations(): Observable<any> {
    return this.http.get(`/applications/vacations`)
  }

  getBusinessTrip(data?: {page: number, size: number, approved?: boolean}): Observable<any> {
    return this.http.get(`/applications/business-trip`, { params: data })
  }

  submit(data): Observable<any> {
    const formData = new FormData();
    for (const file in data) {
      if(data[file]?.value) {
        formData.append(file, data[file].value)
      } else if(file === 'children') {
          formData.append('children', JSON.stringify(data[file]));
      } else if(file === 'education') {
          formData.append('education', JSON.stringify(data[file]))
      } else {
        formData.append(file, data[file])
      }

    }
    return this.http.post(`/applications/candidates`, formData)
  }

  createVacation(data): Observable<any> {
     return this.http.post(`/applications/vacations`, data)
  }

  createBusinessTrip(data): Observable<any> {
    return this.http.post(`/applications/business-trip`, data)
 }

 createDismissal(data): Observable<any> {
  let formData = new FormData();
  for (let i in data) {
    formData.append(i, data[i])
  }
  return this.http.post(`/applications/dismissal`, formData )
 }

 createPromotion(data): Observable<any> {
  let formData = new FormData();
  for (let i in data) {
    formData.append(i, data[i])
  }
  return this.http.post(`/applications/promotion`, formData )
 }

 createAbsence(data): Observable<any> {
  let formData = new FormData();
  for (let i in data) {
    formData.append(i, data[i])
  }
  return this.http.post(`/applications/absence`, formData )
 }

}
