import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

export interface NewsDto {
  id: number;
  title: string;
  description: string;
  coverPath: string;
  creationDate: string;
  publishDate: string;
  html: string;
  approve: boolean;
}

export interface PagedResponse<T> {
  items: T[];
  size: number;
  count: number;
  page: number;
  pageTotal: number;
}

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  constructor(private http: HttpClient) { }

  getNews(data?: {page: number, size: number, approved?: boolean}) {
    return this.http.get<PagedResponse<NewsDto>>(`/news`, { params: data });
  }
}
