import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PublicProcurementsService {

  constructor(private http: HttpClient) { }

  getPublicProcurements(filter: {page: number, size: number}): Observable<any> {
    return this.http.get('/applications/state-purchase', {params: filter});
  }

  getPublicPersonalInfo(id: number): Observable<any> {
    return this.http.get(`/applications/state-purchase/${id}`);
  }


  getListItems(): Observable<any> {
    return this.http.get('/applications/state-purchase/items');
  }

  getCategories(id: number): Observable<any> {
    return this.http.get(`/applications/state-purchase/categories?itemId=${id}`);
  }

  submitNewProcurements(form): Observable<any> {
    let formData = new FormData
    for (let i in form) {
      if (typeof form[i] === 'object') {
        form[i].forEach(file => formData.append('files', file))
      } else {
        formData.append(i, form[i])
      }
  }
  return this.http.post('/applications/state-purchase', formData);
}
}