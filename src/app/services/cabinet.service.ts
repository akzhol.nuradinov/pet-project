import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CabinetService {

  constructor(private http: HttpClient) { }

  getUser(): Observable<any> {
    return this.http.get('/account')
  }

  passwordChange(data: object): Observable<any> {
    return this.http.post('/account/change-password', data)
  }

  getEdsKey(): Observable<any> {
    return this.http.get('/eds')
  }

  sendEdsKey(files: File[]): Observable<any> {
    const formData = new FormData()
    files.forEach(file => formData.append('file', file))
    return this.http.post('/eds', formData)
  }

  deleteEdsKey(id: number): Observable<any> {
    return this.http.delete(`/eds/${id}`)
  }
}
