import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { ApiGuard } from './services/api.guard'
import { HeaderComponent } from './shared/components/header/header.component'
import { LoginComponent } from './shared/components/login/login.component'
import { TechicalSupportDetailComponent } from './sections/admin/components/technical-support/techical-support-detail/techical-support-detail.component'
import { TechicalSupportListComponent } from './sections/admin/components/technical-support/techical-support-list/techical-support-list.component'
import { TechnicalSupportComponent } from './sections/admin/components/technical-support/technical-support.component'
import { CalendarComponent } from './shared/components/calendar/calendar.component'
import { EventCalendarComponent } from './shared/components/calendar/event-calendar/event-calendar.component'
import { SystemControlComponent } from './shared/components/system-control/system-control.component'
import { PreviewComponent } from './shared/components/preview/preview.component'

const routes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'header', component: HeaderComponent },
    { path: 'dashboard', loadChildren: () => import('./sections/dashboard/dashboard.module').then((m) => m.DashboardModule), canActivate: [ApiGuard] },
    { path: 'admin', loadChildren: () => import('./sections/admin/admin.module').then((m) => m.AdminModule), canActivate: [ApiGuard] },
    { path: 'cabinet', loadChildren: () => import('./shared/modules/cabinet/cabinet.module').then((m) => m.CabinetModule), canActivate: [ApiGuard] },
    { path: 'customer', loadChildren: () => import('./sections/customers/customer.module').then((m) => m.CustomerModule), canActivate: [ApiGuard] },
    { path: 'employee', loadChildren: () => import('./sections/employees/employee.module').then((m) => m.EmployeeModule), canActivate: [ApiGuard] },
    { path: 'human-resource', loadChildren: () => import('./sections/human-resource/human-resource.module').then((m) => m.HumanResourceModule), },
    { path: 'public-procurement', loadChildren: () => import('./sections/public-procurement/public-procurement.module').then((m) => m.PublicProcurementModule), },
    { path: "system-control", component: SystemControlComponent},
    { path: 'technical-support', component: TechnicalSupportComponent, children : [
        { path: '', component: TechicalSupportListComponent},
        { path: ':id', component: TechicalSupportDetailComponent},
    ] },
    { path: "calendar", component: CalendarComponent, children : [
        { path: '', component: EventCalendarComponent}
    ]},
    // { path: "preview", component: PreviewComponent},
    { path: '**', redirectTo: '', pathMatch: 'full' }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
