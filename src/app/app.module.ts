import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient, HttpHeaders } from '@angular/common/http'
import { LOCALE_ID, NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { BrowserModule } from '@angular/platform-browser'
import { AppRoutingModule } from './app-routing.module'
import { AdminModule } from './sections/admin/admin.module'
import { ApiInterceptor } from './services/api.interceptor'
import { LoginComponent } from './shared/components/login/login.component'
import localeRu from '@angular/common/locales/ru'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgbModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap'
import { ToastrModule } from 'ngx-toastr'
import { AppComponent } from './app.component'
import { CustomerModule } from './sections/customers/customer.module'
import { EmployeeModule } from './sections/employees/employee.module'
import { SharedModule } from './shared/shared.module'
import { registerLocaleData } from '@angular/common'
import { HumanResourceModule } from './sections/human-resource/human-resource.module'
import { PublicProcurementModule } from './sections/public-procurement/public-procurement.module'
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { DashboardModule } from './sections/dashboard/dashboard.module'

registerLocaleData(localeRu);
export function HttpLoaderFactory(http: HttpClient) {
  const path = window.location.origin + '/assets/i18n/';
  return new TranslateHttpLoader(http, '/assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    AdminModule,
    HttpClientModule,
    LoadingBarHttpClientModule,
    CustomerModule,
    SharedModule,
    DashboardModule,
    NgbPopoverModule,
    HumanResourceModule,
    PublicProcurementModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    EmployeeModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true
    },
    {
      provide: LOCALE_ID,
      useValue: 'ru-RU'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
