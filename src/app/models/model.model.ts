export interface IKeyEds {
	id: number,
	name: string
}

export interface ICustomHttpErrorResponse {
	readonly message: string
	readonly title: string
	readonly status: number
}