export interface IUserEMPloyee {
    id: number,
    birthDate: string,
    authorities: string,
    createdBy: string,
    createdDate: string,
    email: string,
    firstName: string,
    iin: number,
    langKey: string
    lastModifiedBy: string,
    lastModifiedDate: string,
    lastName: string,
    patronymic: string,
    phoneNumber: string
}

export interface Permission {
    AssignApplicationNumber: boolean,
    AssignEmployee: boolean,
    CreateConclusion: boolean,
    DownloadDocuments: boolean,
    ManageApplication: boolean,
    ManageDocuments: boolean,
    SendApplication: boolean,
    RevisionConclusion: boolean
}

export interface UserList {
    count: number,
    items: any[],
    page: number,
    pageTotal: number,
    size: number
}
