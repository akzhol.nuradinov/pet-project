import { PhoneRegularExpression } from './index'

describe('AddNewEmployeeModalComponent', () => {
  const validPhones = [
    "+7747-1112211",
    "+77471112211",
    "+774711122-11",
    "+7(747)111-22-11",
    "+7(747 111-22-11",
    "+7 (747) 111-22-11",
    "8-747-111-22-11",
    "+77 47 111-22-11",
    "77 47 111-22-11",
  ];
  for (const phone of validPhones) {
    it('Valid phones: ' + phone, () => {
      expect(PhoneRegularExpression.test(phone)).toBeTrue();
    });
  }

  const invalidPhones = [
      "+7747-111211",
      "8-747-111-22.11",
      "8-747-111-22-1",
      "9-747-111-22-11",
  ];
  for (const phone of invalidPhones) {
    it('Invalid phone: ' + phone, () => {
      expect(PhoneRegularExpression.test(phone)).toBeFalse();
    });
  }
});
