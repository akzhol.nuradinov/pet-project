
export function cleanNullProps(obj: object, nested?: boolean) {
    for (const field of Object.keys(obj)) {
        if (nested && (obj[field] !== null && typeof obj[field] === 'object')) {
            cleanNullProps(obj[field], nested)
        }
        if (obj[field] === null || obj[field] === 'undefined' || obj[field] === "" || obj[field] === null || obj[field]?.length === 0) {
            delete obj[field]
        }
    }
    return obj
}

export function passwordMatch(password: string, confirm_password: string) {

    return () => {
        if (password === confirm_password) {
            return false
        }

        return true
    }
}

export function debounce(fn: Function, delay: number): Function {
    let timeout;
    return () => {
        const fnCall = () => {fn.apply(this, arguments);}

        clearTimeout(timeout);

        timeout = setTimeout(fnCall, delay)
    };
}

/**
 * This is trick to make Regexes in JavaScript commentable
 * and ignore whitespaces in regexes to make them slightly easier to parse.
 */
function verboseRegExp(input) {
  if (input.raw.length !== 1) {
    throw Error("verboseRegExp: interpolation is not supported");
  }

  let source = input.raw[0];
  let regexp = /(?<!\\)\s|[/][/].*|[/][*][\s\S]*[*][/]/g;
  let result = source.replace(regexp, '');

  return new RegExp(result);
}

export const PhoneRegularExpression = new RegExp(verboseRegExp`
  ^(
    ( (\+?\s*7|8) [(\-\s] *\d{3} | (\+?\s*77|8) [(\-\s] *\d{2})
      [)\-\s]* \d{3} [\-\s] *\d{2} [\-\s]* \d{2} [\-\s]*
  )$
`);
