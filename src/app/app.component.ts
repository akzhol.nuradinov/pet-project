import { Component, OnInit  } from '@angular/core'
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from './services/employee.service';
import { AuthService } from './shared/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  form: FormGroup;
  dictTypes: any;
  constructor(private modalService: NgbModal,
              private toastrService: ToastrService,
              public authService: AuthService,
              private employeeService: EmployeeService,
              private translateService: TranslateService,
              private fb: FormBuilder) {
                this.translateService.setDefaultLang('ru');
                this.translateService.use(sessionStorage.getItem('lang') || "ru");
               }

  ngOnInit(): void {
    this.form = this.fb.group({
      message: [''],
      typeId: [''],
      documents: this.fb.array([])
    });
    if (!!this.authService.token) {
    this.employeeService.dictType().subscribe(dictType => {
      this.dictTypes =  dictType
    });
    }

    // if (environment.production) {
    //   document.addEventListener('contextmenu', event => event.preventDefault());
    // }

  }

  supportModal(support): void {
    this.modalService.open(support, {size: 'medium'});
  }

  onSubmit(): void {
    this.employeeService.techSupport(this.form.value).subscribe((data) => {
      this.toastrService.success('Отправлено')
      this.modalService.dismissAll()
    }, err => {
      this.toastrService.error(err?.error?.detail)
    })
  }

  fileChange(event): void {
    if(event.target.files) {
      for (let file of event.target.files) {
       (<FormArray>this.form.get('documents')).push(new FormControl(file)); 
      }
    }
  }

  toggle(popup) {
    if(popup.isOpen()) {
      popup.close();
    } else {
      popup.open()
    }
  }

  close(): void {
    this.modalService.dismissAll()
  }
 }
