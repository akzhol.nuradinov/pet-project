import { CommonModule, DecimalPipe } from '@angular/common'
import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { NgbModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap'
import { SharedModule } from './../../shared/shared.module'
import { FormsModule } from '@angular/forms';
import { DefaultPageComponent } from 'src/app/shared/components/default-page/default-page.component'
import { InstructionsPageComponent } from './instructions-page/instructions-page'
import { DashboardPageComponent } from './dashboard-page/dashboard-page'
import { TranslateModule } from '@ngx-translate/core'

const routes: Routes = [
  {
    path: '', component: DefaultPageComponent, children: [
      { path: 'main', component: DashboardPageComponent },
      { path: 'instructions', component: InstructionsPageComponent }
    ]
  }
]

@NgModule({
  declarations: [
    DashboardPageComponent,
    InstructionsPageComponent,
  ],
  exports: [],
  imports: [
      CommonModule,
      NgbModule,
      SharedModule,
      FormsModule,
      TranslateModule,
      NgbPaginationModule,
      NgbTypeaheadModule,
      DecimalPipe,
      RouterModule.forChild(routes),
  ]
})

export class DashboardModule { }
