import { Component, OnInit } from "@angular/core";
import { NewsDto, NewsService } from "src/app/services/news.service";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.html',
  //styleUrls: ['./dashboard-page.scss']
})
export class DashboardPageComponent implements OnInit {
  news: NewsDto[];
  loading: number = 0;
  constructor(private newsService: NewsService) {}

  ngOnInit(): void {
    this.loadNews();
  }

  private loadNews() {
    this.loading++;
    this.newsService.getNews().subscribe({
      next: response => {
        this.news = response.items;
        this.loading--;
      },
      error: err => {
        this.loading--;
      }
    });
  }
}
