import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomerService } from 'src/app/services/customer.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {

  form: FormGroup;
  step = true;
  activityTypes: any;
  legalStatus: string;



  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private toastrService: ToastrService,
              private router: Router,
              private customerService: CustomerService,
              private notificationsService: NotificationsService) {
    this.form = this.fb.group({
      firstStep: this.fb.group({
        lastName: [null, [Validators.required]],
        firstName: [null, [Validators.required]],
        patronymic: [null],
        phoneNumber: ['+77', [Validators.required, Validators.minLength(12)]],
        email: [null, [Validators.required, Validators.email]],
        iin: [null, [Validators.required, Validators.minLength(12)]],
        organizationName: [null, [Validators.required]],
        activityType: [null, Validators.required],
        // clientType: [null, Validators.required],
      }),
      secondStep: this.fb.group({
        appealLetter: [null, [Validators.required]],
        application: [null, Validators.required],
        regulation: [null, Validators.required],
        associationMemorandum: [null, Validators.required],
        legalEntityStateRegistrationCertificate: [null, Validators.required],
        financialReport: [null, Validators.required],
        creditBureauReport: [null, Validators.required],
        businessPlan: [null, Validators.required],
        commercialOffer: [null, Validators.required],
        financialSecurityDocument: [null, Validators.required],
        collateralDocument: [null, Validators.required],
        taxReturn: [null, Validators.required],
        idDocuments: [null, Validators.required],
        projectPresentation: [null, Validators.required],
        taxPentionDebtAbsenceCertificate: [null, Validators.required],
        loanDebtAbsenceCertificate: [null, Validators.required],
        propertyCertificate: [null, Validators.required],
        sanitaryPassports: [null, Validators.required],
        budgetTechPlan: [null, Validators.required],
        sketchPlan: [null, Validators.required],
        legalStatusDocuments: [null, Validators.required],
        landExecDecision: [null, Validators.required],
        generalMeetingProtocol: [null, Validators.required],
        khIpIdDocumnets: [null, Validators.required],
        commercialOfferPrice: [null, Validators.required],
      }),
    });
  }

  ngOnInit(): void {
    this.init();
  }

  init() {
    this.legalStatus = this.authService.legalStatus;
    return this.customerService.getActivityTypes().subscribe((activityTypes) => {
      this.activityTypes = activityTypes;
    })
  }


  onSubmit() {
    this.customerService.request(cleanNullProps(this.form.value, true)).subscribe({
      next: (data) => {
        this.toastrService.success(data?.message ? data?.message : 'Заявка отправлена');
        setTimeout(() => this.router.navigate(['/customer/my-applications']), 1000);
      },error: err => {
        this.notificationsService.reportError(err);
      }
    })
  }

  proceed() {
    this.step = !this.step;
  }

  get legalDisable() {
    return (
      this.secondGroup.controls['regulation'].valid &&
      this.secondGroup.controls['associationMemorandum'].valid &&
      this.secondGroup.controls['legalEntityStateRegistrationCertificate'].valid &&
      this.secondGroup.controls['financialReport'].valid &&
      this.secondGroup.controls['commercialOffer'].valid &&
      this.secondGroup.controls['idDocuments'].valid &&
      this.secondGroup.controls['propertyCertificate'].valid &&
      this.secondGroup.controls['sanitaryPassports'].valid &&
      this.secondGroup.controls['budgetTechPlan'].valid &&
      this.secondGroup.controls['sketchPlan'].valid
    );
  }

  get personalDisable() {
    return (
      this.secondGroup.controls['legalStatusDocuments'].valid &&
      this.secondGroup.controls['landExecDecision'].valid &&
      this.secondGroup.controls['generalMeetingProtocol'].valid &&
      this.secondGroup.controls['khIpIdDocumnets'].valid &&
      this.secondGroup.controls['commercialOfferPrice'].valid
    )
  }

  get secondGroup() {
    return this.form.controls['secondStep'] as FormGroup;
  }

  get validate() {
    return (
      this.form.controls['firstStep'].valid
    )
  }

  onFileChange(event, string) {
    let file = null;
    if (event.target.files.length>0 && event.target.files.length) {
      file = event.target.files[0]
      this.getDate().controls[string].setValue(file)
    }
  }


  getDate() {
    return this.form.controls['secondStep'] as FormGroup
  }

}
