import { ComponentFixture, TestBed, tick } from "@angular/core/testing";
import { ApplicationFormComponent } from "./application-form.component";
import { ToastrModule } from "ngx-toastr";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from "@angular/common/http";
import { AdminModule } from "src/app/sections/admin/admin.module";
import { TranslateModule } from "@ngx-translate/core";
import { ReactiveFormsModule } from "@angular/forms";


describe('ApplicationFormComponent', () => {
  let component: ApplicationFormComponent;
  let fixture: ComponentFixture<ApplicationFormComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ AdminModule, HttpClientTestingModule, ReactiveFormsModule, ToastrModule.forRoot(), TranslateModule.forRoot() ],
      // provide the component-under-test and dependent service
      providers: [
        ApplicationFormComponent,
      ],
      declarations: [ApplicationFormComponent]
    }).compileComponents();
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    // inject both the component and the dependent service.
    fixture = TestBed.createComponent(ApplicationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterEach(() => httpTestingController.verify());

  it('Initially page status', () => {
    const req = httpTestingController.expectOne('/dict-activity-types');
    expect(req.request.method).toEqual('GET');
    req.flush([]);

    const componentElement: HTMLElement = fixture.nativeElement;
    const page = new AddApplicationPage(componentElement);
    const nextButton = page.nextButton;
    expect(page.lastName).withContext("No LastName field found").not.toBeNull();
    expect(page.firstName).withContext("No firstName field found").not.toBeNull();
    expect(page.patronymic).withContext("No patronymic field found").not.toBeNull();
    expect(page.phoneNumber).withContext("No phoneNumber field found").not.toBeNull();
    expect(page.email).withContext("No email field found").not.toBeNull();
    expect(page.iin).withContext("No iin field found").not.toBeNull();
    expect(page.organizationName).withContext("No organizationName field found").not.toBeNull();
    expect(page.organizationType).withContext("No organizationType field found").not.toBeNull();
    expect(nextButton).withContext("No Next button found").not.toBeNull();
    expect(nextButton.getAttribute('disabled')).withContext("disabled attribute").not.toBeNull();
  });

  it('Invalid phone', () => {
    const req = httpTestingController.expectOne('/dict-activity-types');
    expect(req.request.method).toEqual('GET');
    req.flush([]);

    const componentElement: HTMLElement = fixture.nativeElement;
    const page = new AddApplicationPage(componentElement);

    page.phoneNumberValue = "+777";
    fixture.detectChanges();

    expect(page.phoneNumber).withContext("No phoneNumber field found").not.toBeNull();
    expect(page.phoneNumberIsValidField).withContext("Phone number is invalid").toBeFalse();
    expect(page.nextButton.getAttribute('disabled')).withContext("disabled attribute").not.toBeNull();
  });

  const validPhones = ["+7747-1112211","+7(747)111-22-11","+7 (747) 111-22-11","8-747-111-22-11"];
  for (const phone of validPhones) {
    it('Valid phone: ' + phone, () => {
      const req = httpTestingController.expectOne('/dict-activity-types');
      expect(req.request.method).toEqual('GET');
      req.flush([]);

      const componentElement: HTMLElement = fixture.nativeElement;
      const page = new AddApplicationPage(componentElement);

      page.phoneNumberValue = phone;
      fixture.detectChanges();

      expect(page.phoneNumber).withContext("No phoneNumber field found").not.toBeNull();
      expect(page.phoneNumberIsValidField).withContext("Phone number valid").toBeTrue();
      expect(page.nextButton.getAttribute('disabled')).withContext("disabled attribute").not.toBeNull();
    });
  }
});

class AddApplicationPage {
  constructor (private componentElement: HTMLElement) {}

  get nextButton() {
    return this.componentElement.querySelector('button')!
  }

  get lastName() {
    return this.componentElement.querySelector('[id=lastName]')!
  }

  get firstName() {
    return this.componentElement.querySelector('[id=firstName]')!
  }

  get patronymic() {
    return this.componentElement.querySelector('[id=patronymic]')!
  }

  get phoneNumber() {
    return this.componentElement.querySelector('[id=phoneNumber]')! as HTMLInputElement
  }

  set phoneNumberValue(newValue: string) {
    this.phoneNumber.value = newValue;
    this.phoneNumber.dispatchEvent(new Event('blur'));
    this.phoneNumber.dispatchEvent(new Event('input'));
  }

  get phoneNumberIsValidField(): boolean {
    return this.phoneNumber.classList.contains("ng-valid")
      && !this.phoneNumber.classList.contains("ng-invalid");
  }

  get email() {
    return this.componentElement.querySelector('[id=email]')!
  }

  get iin() {
    return this.componentElement.querySelector('[id=iin]')!
  }

  get organizationName() {
    return this.componentElement.querySelector('[id=nameOrgan]')!
  }

  get organizationType() {
    return this.componentElement.querySelector('[id=kindOrgan]')!
  }

  get nextButtonIsDisabled() {
    const attribute = this.nextButton.getAttribute('disabled');
    return attribute !== null && attribute !== undefined;
  }
}
