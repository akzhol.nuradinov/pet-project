import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, map, Subscription } from 'rxjs';
import { UserList } from 'src/app/models/user.model';
import { CustomerService } from 'src/app/services/customer.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { cleanNullProps } from 'src/app/utils';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  $subscription: Subscription;
  form: FormGroup;
  userList: UserList;
  column = 'iin'
  direction = 'asc';
  loading = 0;
  constructor(private customerService: CustomerService,
              private employeeService: EmployeeService,
              private fb: FormBuilder,
              private notificationsService: NotificationsService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    })

    this.init()

    this.$subscription = this.employeeService.subject
    .pipe(map((i: any) => i), debounceTime(500))
    .subscribe(data => {
      if(!this.form.get('search')) {
       this.form.addControl('search', new FormControl(data))
       this.init()
       this.form.removeControl('search')
       } else {
          this.form.patchValue({search: data})
          this.init();
          this.form.removeControl('search')
        }
    })

  }

  init(): void {
    this.loading++;
    this.customerService.getUsers(cleanNullProps(this.form.value)).subscribe({
      next: (data) => {
        this.userList = data;
        this.loading--;
      },
      error: err => {
        this.notificationsService.reportError(err);
        this.loading--;
      }
    })
  }

  sort(key: string): any {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      this.direction = 'asc'
    }
    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.init()
    }

  setPage(event) {
    this.form.patchValue({
      page: event - 1,
    });
    this.init();
  }
}
