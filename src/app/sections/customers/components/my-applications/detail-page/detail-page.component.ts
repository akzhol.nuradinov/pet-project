import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { saveAs } from 'file-saver'
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DocumnentConclusionFixComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionHistoryComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-history/documnent-conclusion-history.component';


@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.scss']
})
export class DetailPageComponent implements OnInit {

  userInfo: any;
  progress: any;
  userID: number;

  constructor(private activeRoute: ActivatedRoute,
              private toastrService: ToastrService,
              public authService: AuthService,
              private modalService: NgbModal,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.userID = params['taskId'];
      this.employeeService.detail(params['taskId']).subscribe(data => {
        this.userInfo = data;
      }, err => this.toastrService.error('Заявка не найдена'))
    })

    this.applicationProgress()
  }

  applicationProgress() {
    this.employeeService.applicationProgress(this.userID).subscribe(data => {
      this.progress = data
    });
  }

  onSaveDocument(blob, name: string) {
    this.employeeService.downloadDocument(blob.id).subscribe(data => {
     return saveAs(data, name)
    })
  }

  docRevisionFix(id: number) {
    const ref = this.modalService.open(DocumnentConclusionFixComponent)
    ref.componentInstance.documentId = id
  }

  history(id: number) {
    const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
    ref.componentInstance.documentId = id;
  }

}
