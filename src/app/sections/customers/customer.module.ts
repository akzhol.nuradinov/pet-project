import { CommonModule } from '@angular/common';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './../../shared/shared.module';
import { Routes, RouterModule } from '@angular/router';
import { DefaultPageComponent } from './default-page/default-page.component';
import { NgModule } from '@angular/core';
import { ApplicationFormComponent } from './components/application-form/application-form.component';
import { MyApplicationsComponent } from './components/my-applications/my-applications.component';
import { ApplicationTypeComponent } from './components/application-type/application-type.component';
import { PageComponent } from './components/my-applications/page/page.component';
import { DetailPageComponent } from './components/my-applications/detail-page/detail-page.component';

const routes: Routes = [
    {path: '', component: DefaultPageComponent, children: [
        {path: 'my-applications', component: MyApplicationsComponent, children: [
         {path: '', component: PageComponent}, 
         {path: ':taskId', component: DetailPageComponent}   
        ]},
        {path: 'application-type', component: ApplicationTypeComponent},
        {path: 'application-form', component: ApplicationFormComponent},
    ]},
    
]

@NgModule({
    declarations: [
        DefaultPageComponent,
        ApplicationFormComponent,
        MyApplicationsComponent,
        ApplicationTypeComponent,
        PageComponent,
        DetailPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        NgbPaginationModule,
        SharedModule
    ],
    exports: []
})

export class CustomerModule {}