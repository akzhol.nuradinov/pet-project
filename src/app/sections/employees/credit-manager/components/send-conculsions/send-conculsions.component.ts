import { saveAs } from 'file-saver';
import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-send-conculsions',
  templateUrl: './send-conculsions.component.html',
  styleUrls: ['./send-conculsions.component.scss']
})
export class SendConculsionsComponent implements OnInit {
  receiverList: any
  files = [];
  form: FormGroup;
  userId: any

  constructor(private employeeService: EmployeeService,
              private toastrService: ToastrService,
              public activeModal: NgbActiveModal,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      applicationId: this.userId,
      documents: this.fb.array([]),
      comment: [null],
      status: [null, Validators.required],
    })
  }

  get docFiles() {
    return this.form.get('documents') as FormArray;
  }

  fileOnChange(event): void {
    this.files = []
    if (event.target?.files.length > 0) {
      for (let fileData of event.target?.files) {
        this.files.push(fileData?.name)
        this.docFiles.push(new  FormControl(fileData)) 
      }
      
    }
  }

  sendTo(event,id: number): void {
    if (event?.target?.checked) {
      this.form.patchValue({ receiverId: id})
    }
  }

  onFormConclusion() {
    this.employeeService.onForm().subscribe((blob) => {
      saveAs(blob, 'file')
    })
  }


  submit() {
    this.employeeService.submitConclusion(this.form.value).subscribe((data) => {
      this.toastrService.success('Conclusion submitted successfully!', 'Success!')
      this.activeModal.close()
    }, err => {
      this.toastrService.error(err?.error?.detail?? 'Oops something went wrong');
    })
  }

}
