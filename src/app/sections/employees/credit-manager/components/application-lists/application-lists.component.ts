import { Component, OnDestroy, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { debounceTime, map, Subscription } from 'rxjs';
import { UserList } from 'src/app/models/user.model';
import { EmployeeService } from 'src/app/services/employee.service'
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-application-lists',
  templateUrl: './application-lists.component.html',
  styleUrls: ['./application-lists.component.scss']
})
export class ApplicationListsComponent implements OnInit, OnDestroy {
  applications: UserList;
  regions: any
  statuses: any
  subscription: Subscription;
  column = 'iin';
  direction = 'asc';
  loading: number = 0;

  form: FormGroup
  constructor(private employeeService: EmployeeService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    private notificationService: NotificationsService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    })
    this.getUsers()
    this.getResource()

     this.subscription = this.employeeService.subject
      .pipe(map((i: any) => i), debounceTime(500))
      .subscribe(data => {
        if(!this.form.get('search')) {
         this.form.addControl('search', new FormControl(data))
         this.getUsers()
         this.form.removeControl('search')
         } else {
            this.form.patchValue({search: data})
            this.getUsers();
            this.form.removeControl('search')
          }
      })


  }

  getUsers() {
    this.loading++;
    this.employeeService.getUsers(cleanNullProps(this.form.value)).subscribe({
      next: (data) => {
        this.applications = data;
        this.loading--;
      },
      error: (err) => {
        this.loading--;
        this.notificationService.reportError(err);
      }
    });
  }

  onDetail(id) {
    this.employeeService.detail(id).subscribe(() => {
      this.router.navigate([id, 'data-client'], { relativeTo: this.activeRoute })
    })
  }
  getResource() {
    this.loading++;
    this.employeeService.regionsResource().subscribe({
      next: data => {
        this.regions = data;
        this.loading--;
      },
      error: (err) => {
        this.loading--;
        this.notificationService.reportError(err);
      }
    });

    this.loading++;
    this.employeeService.statusesResource().subscribe({
      next: data => {
      this.statuses = data;
      this.loading--;
    },
    error: (err) => {
      this.loading--;
      this.notificationService.reportError(err);
    }
  });

  }

  changeStatus(status): void {
    this.form.addControl('status', new FormControl(status))
    this.getUsers()
    this.form.removeControl('status')
  }

  setPage(event) {
    this.form.patchValue({ page: event - 1 })
    this.getUsers()
  }

  regionSelect(event): void {
    if(event?.target.value !== 'null') {
      this.form.addControl('region', new FormControl(event?.target.value))
      this.getUsers();
      this.form.removeControl('region')
    } else {
      this.form.removeControl('region')
      this.getUsers()
    }

  }

  sort(key: string): any {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      this.direction = 'asc'
    }
    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.getUsers()
    }

    filterDate(event) {
      if (event.target.value) {
           if (!!this.form.value.date) {
        this.form.patchValue({ date: event.target.value })
      } else {
        this.form.addControl('date', new FormControl(event.target.value))
      }
      } else {
        this.form.removeControl('date');
      }

      this.getUsers();
     }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
