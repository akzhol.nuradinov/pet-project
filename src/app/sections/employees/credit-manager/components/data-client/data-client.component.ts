import { CommentsModalComponent } from './../comments-modal/comments-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { saveAs } from 'file-saver'
import { EmployeeService } from './../../../../../services/employee.service'
import { PermissionService } from 'src/app/shared/services/permission.service';
import { ActionModalComponent } from '../action-modal/action-modal.component';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DocumnentConclusionFixComponent } from '../documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionComponent } from '../documnent-conclusion/documnent-conclusion.component';
import { DocumnentConclusionHistoryComponent } from '../documnent-conclusion-history/documnent-conclusion-history.component';

@Component({
  selector: 'app-data-client',
  templateUrl: './data-client.component.html',
  styleUrls: ['./data-client.component.scss'],
})
export class DataClientComponent implements OnInit {
  userList: any
  userId: number;
  status: boolean;
  applicationNumber: number;

  permission: Permission;
  documentId: number;

  constructor(
    private employeeService: EmployeeService,
    private toastrService: ToastrService,
    private permissionService: PermissionService,
    private modalService: NgbModal,
    public authService: AuthService,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe((params: Params) => {
      this.userId = params['taskId']
    })
    this.permissionService.permission(this.userId).subscribe((perm) => {
      this.permission = perm; 
    })
    this.init()
  }

  init() {
      this.employeeService.detail(this.userId).subscribe((data) => {
        this.userList = data;
      })   
  }


  accept() {
   this.employeeService.acceptApplication({status: 1, id: this.userId }).subscribe((data) => {
   })
  }

  cancel() {
    this.employeeService.acceptApplication({status: 11, id: this.userId }).subscribe((data) => {
    }) 
  }

  action() {
   const ref = this.modalService.open(ActionModalComponent, { size: 'lg'})
   ref.componentInstance.id = this.userId;
  }

  verifyDocument(id: number) {
    this.employeeService.verifyDocument({userId: this.userId, id: id}).subscribe((item) => {
      this.toastrService.success('Одобрено')
      this.init();
    }, err => {
      this.toastrService.error(err?.error?.message)
    })
  }

  revisionDocument(id: number) {
    const ref = this.modalService.open(CommentsModalComponent, {size: 'lg'})
    ref.componentInstance.id = id
  }

  onSaveDocument(blob, name: string) {
    this.employeeService.downloadDocument(blob.id).subscribe(data => {
     return saveAs(data, name)
    })
  }

  openModalApplicationNumber(modalInstance) {
    this.modalService.open(modalInstance, {size: 'sm'})
  }

  assignNumberOfApplication() {
    this.employeeService.assignNumberOfApplication({id: this.userId, number: this.applicationNumber}).subscribe(data => {
      this.toastrService.success('Application number assigned successfully!');
      this.modalService.dismissAll();
    })
  }

  docRevision(id: number) {
    const ref =  this.modalService.open(DocumnentConclusionComponent)
    ref.componentInstance.documentId = id;

  }
  docRevisionFix(id: number) {
   const ref = this.modalService.open(DocumnentConclusionFixComponent)
   ref.componentInstance.documentId = id;
    

  }

  history(id: number) {
    const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
    ref.componentInstance.documentId = id;
  }

  getFullName(user): string {
    return (
      (user?.lastName ?? '') + ' ' +
      (user?.firstName ?? '') + ' ' +
      (user?.patronymic ?? '')
    );
  }
}
