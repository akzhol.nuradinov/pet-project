import { SendConculsionsComponent } from './../send-conculsions/send-conculsions.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { OpenAccessComponent } from '../open-access/open-access.component';
import { EmployeeService } from 'src/app/services/employee.service';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { Permission } from 'src/app/models/user.model';

@Component({
  selector: 'app-application-detail',
  templateUrl: './application-detail.component.html',
  styleUrls: ['./application-detail.component.scss'],
})
export class ApplicationDetailComponent implements OnInit {
  userID: number;
  applicationID: string;
  permission: Permission;
  progress: any;
  constructor(private activeRoute: ActivatedRoute,
              private employeeService: EmployeeService,
              private permissionService: PermissionService,
              private modalService: NgbModal,
              private router: Router) {}

  ngOnInit(): void {
    this.activeRoute.params.subscribe((param: Params) => {
      this.userID = param['taskId']
      this.employeeService.detail(this.userID).subscribe(item => 
       this.applicationID = item.number);
    }) 
    this.permissionService.permission(this.userID).subscribe((perm) => {
      this.permission = perm; 
    })
    this.applicationProgress();
  }

  addConclusion() {
   const ref = this.modalService.open(SendConculsionsComponent, {size: 'lg'})
   ref.componentInstance.userId = this.userID
  }

  applicationProgress() {
    this.employeeService.applicationProgress(this.userID).subscribe(data => {
      this.progress = data
    });
  }

  openAccess() {
    const ref = this.modalService.open(OpenAccessComponent, {size: 'lg'})
    ref.componentInstance.userId = this.userID
  }
}
