import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-documnent-conclusion-fix',
  templateUrl: './documnent-conclusion-fix.component.html',
  styleUrls: ['./documnent-conclusion-fix.component.scss']
})
export class DocumnentConclusionFixComponent implements OnInit {
  form: FormGroup;
  documentId: any
  files = [];
  
  constructor(private activeModal: NgbActiveModal,
    private toastrService: ToastrService,
    private employeeService: EmployeeService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      documents: this.fb.array([])
    });
  }

  get docFiles() {
    return this.form.get('documents') as FormArray;
  }

  fileOnChange(event): void {
    this.files = []
    if (event.target?.files.length > 0) {
      for (let fileData of event.target?.files) {
        this.files.push(fileData?.name)
        this.docFiles.push(new  FormControl(fileData)) 
      }
      
    }
  }

  fixDocument() {
    this.employeeService.refixDocument({id: this.documentId, document: this.form.value.documents}).subscribe((item) => {
      this.toastrService.success('Успешно')
      this.form.reset();
      this.activeModal.close()
    })
  }


  close(): void {
    this.activeModal.close();
  }

}
