import { EmployeeService } from 'src/app/services/employee.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-application-history',
  templateUrl: './application-history.component.html',
  styleUrls: ['./application-history.component.scss']
})
export class ApplicationHistoryComponent implements OnInit {
  userId: number;
  histories: any

  constructor(private activeRoute: ActivatedRoute,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe((params: Params) => {
      this.userId = params['taskId']
      this.employeeService.getHistorList(params['taskId']).subscribe((data) => {
        this.histories = data;
      })
    })
  }

  getFullName(user): string {
    return (
      (user?.receiver?.lastName ?? '') +
      (user?.receiver?.firstName ?? '') +
      (user?.receiver?.patronymic ?? '')
    );
  }
}
