import { Component, OnInit } from '@angular/core'
import { Chart, registerables } from 'chart.js'

@Component({
  selector: 'app-statistics-budged',
  templateUrl: './statistics-budged.component.html',
  styleUrls: ['./statistics-budged.component.scss'],
})
export class StatisticsBudgedComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
    Chart.register(...registerables)
    // const ctx = document.getElementById('myChart');

    const data = {
      labels: [1, 2, 3, 5, 6, 7, 8],
      datasets: [{
        axis: 'y',
        label: 'My First Dataset',
        data: [65, 59, 80, 81, 56, 55, 40],
        fill: false,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)'
        ],
        borderColor: [
          'rgb(255, 99, 132)',
          'rgb(255, 159, 64)',
          'rgb(255, 205, 86)',
          'rgb(75, 192, 192)',
          'rgb(54, 162, 235)',
          'rgb(153, 102, 255)',
          'rgb(201, 203, 207)'
        ],
        borderWidth: 1
      }]
    }
    new Chart('myChart', {
      type: 'line',
      data: data,
      options: {
        scales: {
          y: {
            stacked: true
          }
        }
      }
    })
  }
}
