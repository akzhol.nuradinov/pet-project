import { EmployeeService } from './../../../../../services/employee.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { saveAs } from 'file-saver'
import { PermissionService } from 'src/app/shared/services/permission.service';
import { Permission } from 'src/app/models/user.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DocumnentConclusionComponent } from '../documnent-conclusion/documnent-conclusion.component';
import { DocumnentConclusionFixComponent } from '../documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionHistoryComponent } from '../documnent-conclusion-history/documnent-conclusion-history.component';
import { FormGroup } from '@angular/forms';
import { DocumnentConclusionEditComponent } from '../documnent-conclusion-edit/documnent-conclusion-edit.component';

@Component({
  selector: 'app-my-documents',
  templateUrl: './my-documents.component.html',
  styleUrls: ['./my-documents.component.scss'],
})
export class MyDocumentsComponent implements OnInit {
  userList: any;
  myConclusion: boolean = true;
  taskId: number;
  conclusionId: number;
  permission: Permission;
  form: FormGroup;

  constructor(
    private activeRoute: ActivatedRoute,
    private modalService: NgbModal,
    public authService: AuthService,
    private permissionService: PermissionService,
    private employeeService: EmployeeService
  ) {}

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe((params: Params) => {
      this.taskId = params['taskId'];
      this.init(params['taskId'])
    });

    this.permissionService.permission(this.taskId).subscribe((perm) => {
      this.permission = perm; 
    })

  }

  init(id: number) {
    this.employeeService.myConclusions(id, {mine: this.myConclusion}).subscribe((data) => {
      this.userList = data;
    });
  }

  save(blob, name: string) {
    this.employeeService.downloadDocument(blob.id).subscribe(data => {
      return saveAs(data, name)
     })
  }

  repair(id: number): void {
    this.employeeService.repair(id).subscribe(data => {
      this.init(this.taskId)
    })
  }

  getFullName(user): string {
    return (
      (user?.sender?.lastName ?? '') + ' ' +
      (user?.sender?.firstName ?? '') + ' ' +
      (user?.sender?.patronymic ?? '')
    );
  }
  
  close() {
    this.modalService.dismissAll()
  }

  docRevision(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionComponent)
   ref.componentInstance.documentId = id;

  }

  docRevisionfix(id?: number) {
    const ref = this.modalService.open(DocumnentConclusionFixComponent)
    ref.componentInstance.documentId = id;
 
   }

   applicationRevision(template) {
    const ref = this.modalService.open(DocumnentConclusionEditComponent)
    ref.componentInstance.template = template
   }

  history(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
   ref.componentInstance.documentId = id;
  }
}
