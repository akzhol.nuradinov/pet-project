import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-documnent-conclusion-history',
  templateUrl: './documnent-conclusion-history.component.html',
  styleUrls: ['./documnent-conclusion-history.component.scss']
})
export class DocumnentConclusionHistoryComponent implements OnInit {
  documentId: any
  histories = [];
  constructor(private activeModal: NgbActiveModal,
    private employeeService: EmployeeService,) { }

  ngOnInit(): void {
    this.employeeService.historyDocument(this.documentId).subscribe(data => {
      this.histories = data
    });    
  }

  close(): void {
    this.activeModal.close();
  }

}
