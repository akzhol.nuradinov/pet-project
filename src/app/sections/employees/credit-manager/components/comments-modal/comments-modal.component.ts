import { ToastrService } from 'ngx-toastr';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-comments-modal',
  templateUrl: './comments-modal.component.html',
  styleUrls: ['./comments-modal.component.scss']
})
export class CommentsModalComponent implements OnInit {
  id: any
  form: FormGroup;

  constructor(private employeeService: EmployeeService,
              private toatrService: ToastrService,
              private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      comments: new FormControl()
    })
  }

  close() {
    this.activeModal.close()
  }
  

  revisionDocument() {
    this.employeeService.revisionDocument({id: this.id, comments: this.form.value.comments}).subscribe((item) => {
      this.toatrService.success('Sent')
      this.activeModal.close()
    })
  }
}
