import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-documnent-conclusion-edit',
  templateUrl: './documnent-conclusion-edit.component.html',
  styleUrls: ['./documnent-conclusion-edit.component.scss']
})
export class DocumnentConclusionEditComponent implements OnInit {
  template: any;
  form: FormGroup;
  constructor(private fb: FormBuilder,
    private employeeService: EmployeeService,
    private toastrService: ToastrService,
    private activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      comment: [null],
      status: [null, Validators.required],
    })

    if(!!this.template) {
      this.form.patchValue({
        status: this.template?.conclusionStatus.name
      })
    }
  }

  close(): void {
    this.activeModal.close();
  }

  submit() {
    this.employeeService.updateConclusion(cleanNullProps(this.form.value),this.template?.id).subscribe((data) => {
      this.toastrService.success(data?.message)
      this.activeModal.close();
    }, err => {
      this.toastrService.error(err?.error?.detail);
    })
  }

}
