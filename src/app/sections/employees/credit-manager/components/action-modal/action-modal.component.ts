import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { IUserEMPloyee } from 'src/app/models/user.model'
import { EmployeeService } from 'src/app/services/employee.service'
import { PermissionService } from 'src/app/shared/services/permission.service'
@Component({
  selector: 'app-action-modal',
  templateUrl: './action-modal.component.html',
  styleUrls: ['./action-modal.component.scss']
})
export class ActionModalComponent implements OnInit {
  form: FormGroup
  receiverList: any
  employeesList = [];
  selectedExecutor: any
  id: number

  public isExecuterCollapsed = true;
  public isApplicationsCollapsed = false;
  permission: any



  constructor(private fb: FormBuilder,
    private toastrService: ToastrService,
    private permissionService: PermissionService,
    public activeModal: NgbActiveModal,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      receiverId: [null, Validators.required],
      applicationId: [this.id],
      comment: [null, Validators.required]
    })

    this.employeeService.sendTo(this.id).subscribe((data) =>
      this.receiverList = data
    )

    this.employeeService.getEmployeesList( {applicationId: this.form.value.applicationId} ).subscribe(list =>
      this.employeesList = list
    )
    this.permissionService.permission(this.id).subscribe((perm) => {
      this.permission = perm
    })
  }

  onSelectExecutor(event): void {
    this.selectedExecutor = event.target.value
  }

  applicationAccess() {
    this.employeeService.sendApplication(this.form.value).subscribe((data) => {
      this.toastrService.success(data?.message?.message || 'Направлено')
      this.activeModal.close()
    }, (error) => {
      this.toastrService.error(error?.error?.detail || 'Произошла ошибка')
      this.activeModal.close()
    })
  }

  sendSelectedExecutor(): void {
    this.employeeService.executerTask({ receiverLogin: this.selectedExecutor }, this.id).subscribe(data => {
      this.toastrService.success(data?.message?.message || 'Назначено')
      this.activeModal.close()
    }, error => {
      this.toastrService.error(error?.error?.title || 'Произошла ошибка')
      this.activeModal.close()
    })
  }

  // submit() {
  //   this.employeeService.submitConclusion(this.form.value).subscribe(() => {
  //     this.toastrService.success('Sent')
  //     this.activeModal.close()
  //   })
  // }

  sendTo(event, id: number): void {
    if (event?.target?.checked) {
      this.form.patchValue({ receiverId: id })
    }
  }

  getFullName(u?: IUserEMPloyee): string {
    return (u?.lastName ?? '') + ' ' +
      (u?.firstName ?? '')
  }

}
