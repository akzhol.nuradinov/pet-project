import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EmployeeService } from 'src/app/services/employee.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-open-access',
  templateUrl: './open-access.component.html',
  styleUrls: ['./open-access.component.scss']
})
export class OpenAccessComponent implements OnInit {
receiverList = []
userId: any;
form: FormGroup;
  constructor(public activeModal: NgbActiveModal,
              private fb: FormBuilder,
              private toastrService: ToastrService,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.sendTo(this.userId).subscribe((data) =>
    this.receiverList = data
    )

    this.init()
  }

  init() {
    this.form = this.fb.group({
      applicationId: this.userId,
      receiverId: null
    })
  }

  sendTo(event, id: number) {
    if (event?.target?.checked) {
      this.form.patchValue({ receiverId: id})
    }
  }

  submit() {
    this.employeeService.sendApplication(this.form.value).subscribe(() => {
      this.toastrService.success('Sent')
      this.activeModal.close()
    })
  }

}
