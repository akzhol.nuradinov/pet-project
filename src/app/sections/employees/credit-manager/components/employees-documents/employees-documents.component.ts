import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Params } from '@angular/router'
import { EmployeeService } from 'src/app/services/employee.service'
import { saveAs } from 'file-saver'
import { PermissionService } from 'src/app/shared/services/permission.service'
import { Permission } from 'src/app/models/user.model'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { AuthService } from 'src/app/shared/services/auth.service'
import { cleanNullProps } from 'src/app/utils'
import { DocumnentConclusionFixComponent } from '../documnent-conclusion-fix/documnent-conclusion-fix.component'
import { DocumnentConclusionComponent } from '../documnent-conclusion/documnent-conclusion.component'
import { DocumnentConclusionHistoryComponent } from '../documnent-conclusion-history/documnent-conclusion-history.component'
import { DocumnentConclusionEditComponent } from '../documnent-conclusion-edit/documnent-conclusion-edit.component'


@Component({
  selector: 'app-employees-documents',
  templateUrl: './employees-documents.component.html',
  styleUrls: ['./employees-documents.component.scss'],
})
export class EmployeesDocumentsComponent implements OnInit {
  userList: any
  taskId: number;
  permission: Permission;
  isCollapsed = false;
  form: FormGroup;
  conclusionId: number;
  files = [];
  documentId: number
  constructor(
    private activeRoute: ActivatedRoute,
    private fb: FormBuilder,
    public authService: AuthService,
    private toastrService: ToastrService,
    private permissionService: PermissionService,
    private modalService: NgbModal,
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    this.activeRoute.parent.params.subscribe((params: Params) => {
      this.taskId = params['taskId'];
      this.init(params['taskId'])
    })

    this.permissionService.permission(this.taskId).subscribe((perm) => {
      this.permission = perm; 
    })

    this.form = this.fb.group({
      documents: this.fb.array([]),
      comment: [null],
      status: [null, Validators.required],
    })

  }

  init(id: number) {
    this.employeeService.myConclusions(id).subscribe((data) => {
      this.userList = data;
    })
  }

  save(blob, name: string) {
    this.employeeService.downloadDocument(blob.id).subscribe(data => {
      return saveAs(data, name)
     })
  }
  repair(id: number): void {
    this.employeeService.repair(id).subscribe(data => {
      this.init(this.taskId)
    })
  }

  getFullName(user): string {
    return (
      (user?.sender?.lastName ?? '') + ' ' +
      (user?.sender?.firstName ?? '') + ' ' +
      (user?.sender?.patronymic ?? '')
    )
  }

  onFormConclusion() {
    this.employeeService.onForm().subscribe((blob) => {
      saveAs(blob, 'file')
    })
  }

  submit() {
    this.employeeService.updateConclusion(this.form.value,this.conclusionId ).subscribe((data) => {
      this.toastrService.success(data?.message)
      this.modalService.dismissAll()
    }, err => {
      this.toastrService.error(err?.error?.detail);
    })
  }

  close() {
    this.modalService.dismissAll()
  }

  docRevision(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionComponent)
   ref.componentInstance.documentId = id;

  }

  docRevisionfix(id?: number) {
    const ref = this.modalService.open(DocumnentConclusionFixComponent)
    ref.componentInstance.documentId = id;
 
   }

  applicationRevision(template) {
   const ref = this.modalService.open(DocumnentConclusionEditComponent)
   ref.componentInstance.template = template
  }

  history(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
   ref.componentInstance.documentId = id;
  }
}
