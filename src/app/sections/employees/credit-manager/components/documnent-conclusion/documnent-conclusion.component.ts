import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-documnent-conclusion',
  templateUrl: './documnent-conclusion.component.html',
  styleUrls: ['./documnent-conclusion.component.scss']
})
export class DocumnentConclusionComponent implements OnInit {
  form: FormGroup;
  documentId: any
  constructor(private activeModal: NgbActiveModal,
    private toastrService: ToastrService,
    private employeeService: EmployeeService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      comment: [null]
    });
  }

  revisionDocument() {
    this.employeeService.revisionDocument({id: this.documentId, comments: this.form.value.comment}).subscribe((item) => {
      this.toastrService.success('Успешно')
      this.form.reset();
      this.activeModal.close()
    })
  }

  
  close(): void {
    this.activeModal.close();
  }
}
