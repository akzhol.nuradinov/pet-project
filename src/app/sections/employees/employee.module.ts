import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import {
  NgbAccordionModule,
  NgbCollapseModule,
  NgbDatepickerModule,
  NgbDropdownModule,
  NgbModule,
  NgbPaginationModule,
  NgbTooltipModule
} from '@ng-bootstrap/ng-bootstrap'
import { SharedModule } from 'src/app/shared/shared.module'
import { ApplicationDetailComponent } from './credit-manager/components/application-detail/application-detail.component'
import { ApplicationHistoryComponent } from './credit-manager/components/application-history/application-history.component'
import { ApplicationListsComponent } from './credit-manager/components/application-lists/application-lists.component'
import { DataClientComponent } from './credit-manager/components/data-client/data-client.component'
import { EmployeesDocumentsComponent } from './credit-manager/components/employees-documents/employees-documents.component'
import { MyDocumentsComponent } from './credit-manager/components/my-documents/my-documents.component'
import { PaymentGraphComponent } from './credit-manager/components/payment-graph/payment-graph.component'
import { SendConculsionsComponent } from './credit-manager/components/send-conculsions/send-conculsions.component'
import { StatisticsBudgedComponent } from './credit-manager/components/statistics-budged/statistics-budged.component'
import { PageComponent } from './credit-manager/default-page/page/page.component';
import { OpenAccessComponent } from './credit-manager/components/open-access/open-access.component';
import { CommentsModalComponent } from './credit-manager/components/comments-modal/comments-modal.component';
import { ActionModalComponent } from './credit-manager/components/action-modal/action-modal.component'
import { FormsModule } from '@angular/forms';
import { DocumnentConclusionComponent } from './credit-manager/components/documnent-conclusion/documnent-conclusion.component';
import { DocumnentConclusionFixComponent } from './credit-manager/components/documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionHistoryComponent } from './credit-manager/components/documnent-conclusion-history/documnent-conclusion-history.component';
import { DocumnentConclusionEditComponent } from './credit-manager/components/documnent-conclusion-edit/documnent-conclusion-edit.component'

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      { path: '', component: ApplicationListsComponent },
      { path: 'statistics-budged', component: StatisticsBudgedComponent },
      {
        path: ':taskId',
        component: ApplicationDetailComponent,
        children: [
          { path: 'data-client', component: DataClientComponent },
          { path: 'employee-document', component: EmployeesDocumentsComponent },
          { path: 'my-documents', component: MyDocumentsComponent },
          { path: 'payment-graph', component: PaymentGraphComponent },
          {
            path: 'application-history',
            component: ApplicationHistoryComponent,
          },


        ],

      },

    ],
  },
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    NgbDatepickerModule,
    NgbDropdownModule,
    NgbAccordionModule,
    NgbCollapseModule,
    FormsModule,
    NgbTooltipModule,
    NgbPaginationModule,
    NgbModule
  ],
  declarations: [
    PageComponent,
    ApplicationListsComponent,
    ApplicationDetailComponent,
    DataClientComponent,
    EmployeesDocumentsComponent,
    MyDocumentsComponent,
    ApplicationListsComponent,
    PaymentGraphComponent,
    SendConculsionsComponent,
    StatisticsBudgedComponent,
    OpenAccessComponent,
    CommentsModalComponent,
    ApplicationHistoryComponent,
    ActionModalComponent,
    DocumnentConclusionComponent,
    DocumnentConclusionFixComponent,
    DocumnentConclusionHistoryComponent,
    DocumnentConclusionEditComponent
  ],
  exports: [RouterModule],
})
export class EmployeeModule { }
