import { CommonModule, DecimalPipe } from '@angular/common'
import { NgModule } from "@angular/core"
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { NgbModule, NgbPaginationModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap'
import { SharedModule } from './../../shared/shared.module'
import { AddNewEmployeeModalComponent } from './components/add-new-employee-modal/add-new-employee-modal.component'
import { DetailModalComponent } from './components/detail-modal/detail-modal.component'
import { EmployeesComponent } from './components/employees/employees.component'
import { TableCustomerComponent } from './components/table-customer/table-customer.component'
import { TableEmployeeComponent } from './components/table-employee/table-employee.component'
import { TechicalSupportDetailComponent } from './components/technical-support/techical-support-detail/techical-support-detail.component'
import { TechicalSupportListComponent } from './components/technical-support/techical-support-list/techical-support-list.component'
import { TechnicalSupportComponent } from './components/technical-support/technical-support.component'
import { DefaultPageComponent } from "./default-page/default-page.component";
import { GalleryComponent } from './components/technical-support/gallery/gallery.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PageComponent } from './components/table-customer/page/page.component';
import { PageTableComponent } from './components/table-employee/page/page.component';
import { PersonalInfoComponent } from './components/table-customer/personal-info/personal-info.component'
import { AddRequestModalComponent } from './components/technical-support/add-request-modal/add-request-modal.component'

const routes: Routes = [
    {
        path: '', component: DefaultPageComponent, children: [
            {
                path: '', component: EmployeesComponent, children: [
                    { path: 'employee', component: TableEmployeeComponent, children: [
                        {   path: '', component: PageTableComponent },
                        {   path: ':id', component: DetailModalComponent}
                    ]},
                    { path: 'customer', component: TableCustomerComponent, children: [
                        { path: '', component: PageComponent},
                        { path: ':id', component: PersonalInfoComponent},
                    ] },
                ]
            },
        ]
    }
]

@NgModule({
    declarations: [
        DefaultPageComponent,
        AddNewEmployeeModalComponent,
        TableEmployeeComponent,
        TableCustomerComponent,
        EmployeesComponent,
        DetailModalComponent,
        TechnicalSupportComponent,
        TechicalSupportDetailComponent,
        TechicalSupportListComponent,
        AddRequestModalComponent,
        GalleryComponent,
        PageComponent,
        PersonalInfoComponent,
        PageTableComponent
    ],
    exports: [],
    imports: [
        CommonModule,
        TranslateModule,
        NgbModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        NgbPaginationModule,
        NgbTypeaheadModule,
        DecimalPipe,
        RouterModule.forChild(routes),
    ]
})

export class AdminModule { }
