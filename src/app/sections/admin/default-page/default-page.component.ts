import { Component } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { AddNewEmployeeModalComponent } from './../components/add-new-employee-modal/add-new-employee-modal.component'


@Component({
  selector: 'app-default-page',
  templateUrl: './default-page.component.html',
  styleUrls: ['./default-page.component.scss']
})
export class DefaultPageComponent {

  constructor(private modalService: NgbModal) {
  }

  newEmployee() {
    const ref = this.modalService.open(AddNewEmployeeModalComponent, { size: 'large' })
  }

}
