import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { EmployeeService } from 'src/app/services/employee.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-add-request-modal',
  templateUrl: './add-request-modal.component.html',
  styleUrls: ['./add-request-modal.component.scss']
})
export class AddRequestModalComponent implements OnInit {
  form: FormGroup;
  dictTypes: any;
  public isCollapsed = false;
  constructor(private fb: FormBuilder,
              private modalService: NgbModal,
              private toastrService: ToastrService,
              private employeeService: EmployeeService,
              private translateService: TranslateService,
              private notificationsService: NotificationsService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      typeId: [null, Validators.required],
      message: [null , Validators.required],
      documents: this.fb.array([])
    })
    this.employeeService.dictType().subscribe(dictType => {
      this.dictTypes =  dictType
    });
  }

  submit() {
    this.form.disable();
    this.employeeService.techSupport(this.form.value).subscribe({
      next: (data) => {
        this.form.enable();
        this.employeeService.techSupportRequestCreated.next(true);
        this.toastrService.success(this.translateService.instant("techsupport.success"))
        this.modalService.dismissAll()
      },
      error: err => {
        this.form.enable();
        this.notificationsService.reportError(err);
      }
    })
  }

  fileChange(event): void {
    if(event.target.files) {
      for (let file of event.target.files) {
       (<FormArray>this.form.get('documents')).push(new FormControl(file));
      }
    }
  }
}
