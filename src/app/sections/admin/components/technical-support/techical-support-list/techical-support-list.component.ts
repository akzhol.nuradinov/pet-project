import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AddRequestModalComponent } from '../add-request-modal/add-request-modal.component';
import { EmployeeService } from 'src/app/services/employee.service';

const defaultDescColumns = ['creationDate'];

@Component({
  selector: 'app-techical-support-list',
  templateUrl: './techical-support-list.component.html',
  styleUrls: ['./techical-support-list.component.scss']
})
export class TechicalSupportListComponent implements OnInit {

  listSupports: UserList;
  form: FormGroup;
  column = 'creationDate'
  direction = 'desc';
  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router,
              private modalsService: NgbModal,
              private activeRoute: ActivatedRoute,
              private employeeService: EmployeeService,
              private adminService: AdminService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    });
    this.getList();
    this.employeeService.techSupportRequestCreated.subscribe(() => {
      this.getList();
    })
  }

  filterDate(event) {
    if (event.target.value) {
      if (!!this.form.value.date) {
        this.form.patchValue({ date: event.target.value })
      } else {
        this.form.addControl('date', new FormControl(event.target.value))
      }
    } else {
      this.form.removeControl('date');
    }

    this.getList();
   }

  getList(): void {
    this.adminService.technicalSupport(this.form.value).subscribe((data) => {
      this.listSupports = data;
    })
  }

  sort(key: string): any {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      if (defaultDescColumns.includes(key)) {
        this.direction = 'desc';
      } else {
        this.direction = 'asc'
      }
    }

    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.getList()
  }

    getSortClass(key: string): string | undefined {
        if (this.column === key) {
            if (this.direction === 'asc') {
                return "bi bi-sort-alpha-down";
            } else {
                return "bi bi-sort-alpha-up";
            }
        }

        return undefined;
    }

  setPage(event) {
    this.form.patchValue({
      page: event-1
    });
    this.getList();
  }

  detail(id: number): void {
    if (this.authService.isAdmin) {
      this.router.navigate([id], {relativeTo: this.activeRoute });
    }
  }

  getFullName(item): string {
    return (item.firstName ?? "") +
            ' ' +
            (item.lastName ?? "") +
            ' ' +
            (item?.patronymic ?? '')
  }

  newCandidate(): void {
    this.modalsService.open(AddRequestModalComponent, {size: 'md'})
  }
}
