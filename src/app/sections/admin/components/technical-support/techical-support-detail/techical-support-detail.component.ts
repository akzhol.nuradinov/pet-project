import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import {join} from '@fireflysemantics/join';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';
import { environment } from 'src/environments/environment';
import { GalleryComponent } from '../gallery/gallery.component';

@Component({
  selector: 'app-techical-support-detail',
  templateUrl: './techical-support-detail.component.html',
  styleUrls: ['./techical-support-detail.component.scss']
})
export class TechicalSupportDetailComponent implements OnInit {
  readonly urlApi: string = environment.api;

  techId: number;
  techDetail: any;
  status = [];
  adminList = [];
  assignNumber: number;
  imagesIndex: number = 0;


  constructor(private activeRoute: ActivatedRoute,
              private modalService: NgbModal,
              private toastrService: ToastrService,
              private adminService: AdminService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.techId = params['id'];
    })
    this.getDetailData()
  }
  getDetailData(): void {
    this.adminService.technicalSupportDetail(this.techId).subscribe((data) => {
      this.techDetail = data;
    });

    this.adminService.technicalSupportStatus().subscribe((status) => {
      this.status = status;
    })

    this.adminService.getAdmins().subscribe((admins) => {
      this.adminList = admins
    })

  }

  increment() {
    if (this.imagesIndex < this.techDetail.files.length - 1) {
      this.imagesIndex += 1;
    }
  }

  decrement() {
    if (this.imagesIndex > 0) {
      this.imagesIndex -= 1;
    }
  }

  openGallery(id: number, filename: string): void {
   const ref = this.modalService.open(GalleryComponent, {size: 'xl', centered: true});
   ref.componentInstance.id = id;
   ref.componentInstance.filename = filename;
  }

  getImage(item) {
    return join('http://192.168.3.76:8080' + item.path + item.filename)
  }

  adminChanged(id: string): void {
    this.adminService.assginAdminTechSup(id, this.techId).subscribe(() => {
      this.toastrService.success('Изменен');
    },err => this.toastrService.error('Error')) 
  }

  statusChange(id: string): void {
    this.adminService.assginStatusTechSup(id, this.techId).subscribe(() => {
      this.toastrService.success('Изменен');
    },err => this.toastrService.error('Error'))
  }

  changeNumber(assign): void {
    this.modalService.open(assign, {size: 'sm'});
   }

   assignNumberTech(): void {
    this.adminService.assignNumberTech(this.techDetail?.id, this.assignNumber).subscribe(() => {
      this.toastrService.success('Назначено')
      this.getDetailData()
      this.modalService.dismissAll()
    }, err => this.toastrService.error('Ошибка'))
    }

    getFullName(item): string {
      return (item?.firstName ?? "") +
              ' ' +
              (item?.lastName ?? "") +
              ' ' +
              (item?.patronymic ?? "")
    }

}
