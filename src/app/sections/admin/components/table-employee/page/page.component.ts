import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, map, Subscription } from 'rxjs';
import { AdminService } from 'src/app/services/admin.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { cleanNullProps } from 'src/app/utils';
import { AddNewEmployeeModalComponent } from '../../add-new-employee-modal/add-new-employee-modal.component';
import { DetailModalComponent } from '../../detail-modal/detail-modal.component';
import { UserList } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageTableComponent implements OnInit, OnDestroy {
  id: number = 2;
  userList: UserList;
  form: FormGroup
  $subscription: Subscription;
  $$subscription: Subscription;
  listPositions: any;
  column = 'iin'
  direction = 'asc'
  loading = 0;

  constructor(
    private adminService: AdminService,
    private employeeService: EmployeeService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private router: Router,
    private toastrService: ToastrService,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    })
    this.getUsers()
    this.$subscription = this.adminService.addedUSER.subscribe(
      () => {
        this.getUsers()
      })

      this.loading++;
      this.adminService.sortByPosition().subscribe({
        next: item => {
          this.listPositions = item;
          this.loading--;
        },
        error: err => {
          this.notificationsService.reportError(err);
          this.loading--;
        }
      })

     this.$$subscription = this.employeeService.subject
      .pipe(map((i: any) => i), debounceTime(500))
      .subscribe(data => {
        if(!this.form.get('search')) {
         this.form.addControl('search', new FormControl(data))
         this.getUsers()
         this.form.removeControl('search')
         } else {
            this.form.patchValue({search: data})
            this.getUsers();
            this.form.removeControl('search')
          }
      })
  }

  ngOnDestroy(): void {
    this.$subscription.unsubscribe();
    this.$$subscription.unsubscribe();
  }

  getUsers() {
    this.loading++;
    this.adminService.getEmployees(cleanNullProps(this.form.value)).subscribe({
        next: (data) => {
          this.userList = data;
          this.loading--;
        },
        error: (err) => {
          this.notificationsService.reportError(err);
          this.loading--;
        }
      })
  }

  sort(key: string): any {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      this.direction = 'asc'
    }
    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.getUsers()
  }

    getSortClass(key: string): string | undefined {
        if (this.column === key) {
            if (this.direction === 'asc') {
                return "bi bi-sort-alpha-down";
            } else {
                return "bi bi-sort-alpha-up";
            }
        }

        return undefined;
    }


  onEdit(user: any) {
    const ref = this.modalService.open(AddNewEmployeeModalComponent, {size: 'large',})
    ref.componentInstance.UserEMPloyees = user
  }

  onDetail(user: any) {
    const ref = this.modalService.open(DetailModalComponent, {
      size: 'medium',
    })
    ref.componentInstance.IUserEMPloyee = user
    ref.componentInstance.userType = 'employee'
  }

  onDelete(id: number) {
    this.adminService.deleteUser(id).subscribe((data) => {
      this.toastrService.success(data?.message)
    }, err => {
      this.notificationsService.reportError(err);
    })
  }

  setPage(event) {
    this.form.patchValue({
      page: event - 1,
    })
    this.getUsers()
  }

  onChanged(value: string) {
    if (value) {
      this.router.navigate(['/admin/employee'],{queryParams: {category: value}})
      this.form.addControl('type', new FormControl(value))
      this.form.patchValue({type: value})
    } else {
      this.form.removeControl('type')
      this.router.navigate(['/admin/employee'])

    }

    this.getUsers()
  }

  newEmployee() {
    const ref = this.modalService.open(AddNewEmployeeModalComponent, { size: 'large' })
  }

  getFullName(item): string {
    return (item.firstName ?? "") +
            ' ' +
            (item.lastName ?? "") +
            ' ' +
            (item?.patronymic ?? '')
  }
}


