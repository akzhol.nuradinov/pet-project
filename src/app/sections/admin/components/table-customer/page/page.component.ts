import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { debounceTime, map, Subscription } from 'rxjs';
import { IUserEMPloyee, UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { EmployeeService } from 'src/app/services/employee.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  userList: UserList;
  form: FormGroup
  IUserEMPloyee: IUserEMPloyee
  $subscription: Subscription;
  $$subscription: Subscription;
  column = 'iin'
  direction = 'asc'
  constructor(
    private adminService: AdminService,
    private employeeService: EmployeeService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    })
    this.getUsers()

    this.$subscription = this.adminService.addedUSER.subscribe(() => {
      this.getUsers()
    })

    this.$$subscription = this.employeeService.subject
    .pipe(map((i: any) => i), debounceTime(500))
    .subscribe(data => {
      if(!this.form.get('search')) {
       this.form.addControl('search', new FormControl(data))
       this.getUsers()
       this.form.removeControl('search')
       } else {
          this.form.patchValue({search: data})
          this.getUsers();
          this.form.removeControl('search')
        }
    })
  }

  ngOnDestroy(): void {
    this.$subscription.unsubscribe();
    this.$$subscription.unsubscribe();
  }

  getUsers() {
    this.adminService
      .getClients(cleanNullProps(this.form.value))
      .subscribe((data) => (this.userList = data))
  }

  // onDetail(user: IUserEMPloyee) {
  //   const ref = this.modalService.open(DetailModalComponent, {
  //     size: 'large',
  //   })
  //   ref.componentInstance.IUserEMPloyee = user
  //   ref.componentInstance.userType = 'customer'
  // }

  sort(key: string): any {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      this.direction = 'asc'
    }
    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.getUsers()
    }

  setPage(event) {
    this.form.patchValue({
      page: event - 1,
    })
    this.getUsers()
  }
}
