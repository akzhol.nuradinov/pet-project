import { ComponentFixture, TestBed, tick } from "@angular/core/testing";
import { AddNewEmployeeModalComponent } from "./add-new-employee-modal.component";
import { ToastrModule } from "ngx-toastr";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from "@angular/common/http";
import { AdminModule } from "src/app/sections/admin/admin.module";
import { TranslateModule } from "@ngx-translate/core";
import { ReactiveFormsModule } from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";


describe('AddNewEmployeeModalComponent', () => {
  let component: AddNewEmployeeModalComponent;
  let fixture: ComponentFixture<AddNewEmployeeModalComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ AdminModule, HttpClientTestingModule, ReactiveFormsModule, ToastrModule.forRoot(), TranslateModule.forRoot() ],
      // provide the component-under-test and dependent service
      providers: [
        AddNewEmployeeModalComponent,
        NgbActiveModal,
      ],
      declarations: [AddNewEmployeeModalComponent]
    }).compileComponents();
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    // inject both the component and the dependent service.
    fixture = TestBed.createComponent(AddNewEmployeeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterEach(() => httpTestingController.verify());

  it('Invalid phone', () => {
    const req = httpTestingController.expectOne('/dict-service-groups');
    expect(req.request.method).toEqual('GET');
    req.flush([]);

    const componentElement: HTMLElement = fixture.nativeElement;
    const page = new AddNewEmployeeModal(componentElement);

    page.phoneNumberValue = "+777";
    fixture.detectChanges();

    expect(page.phoneNumber).withContext("No phoneNumber field found").not.toBeNull();
    expect(page.phoneNumberIsValidField).withContext("Phone number is invalid").toBeFalse();
  });

  const validPhones = ["+7747-1112211","+7(747)111-22-11","+7 (747) 111-22-11","8-747-111-22-11"];
  for (const phone of validPhones) {
    it('Valid phone: ' + phone, () => {
      const req = httpTestingController.expectOne('/dict-service-groups');
      expect(req.request.method).toEqual('GET');
      req.flush([]);

      const componentElement: HTMLElement = fixture.nativeElement;
      const page = new AddNewEmployeeModal(componentElement);

      page.phoneNumberValue = phone;
      fixture.detectChanges();

      expect(page.phoneNumber).withContext("No phoneNumber field found").not.toBeNull();
      expect(page.phoneNumberIsValidField).withContext("Phone number valid").toBeTrue();
    });
  }
});

class AddNewEmployeeModal {
  constructor (private componentElement: HTMLElement) {}

  get phoneNumber() {
    return this.componentElement.querySelector('[id=phoneNumber]')! as HTMLInputElement
  }

  set phoneNumberValue(newValue: string) {
    this.phoneNumber.value = newValue;
    this.phoneNumber.dispatchEvent(new Event('blur'));
    this.phoneNumber.dispatchEvent(new Event('input'));
  }

  get phoneNumberIsValidField(): boolean {
    return this.phoneNumber.classList.contains("ng-valid")
      && !this.phoneNumber.classList.contains("ng-invalid");
  }
}
