import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'
import { ToastrService } from 'ngx-toastr'
import { passwordMatch } from 'src/app/utils'
import { IUserEMPloyee } from '../../../../models/user.model'
import { AdminService } from './../../../../services/admin.service'
import { PhoneRegularExpression } from './../../../../utils/index'

@Component({
  selector: 'app-add-new-employee-modal',
  templateUrl: './add-new-employee-modal.component.html',
  styleUrls: ['./add-new-employee-modal.component.scss'],
})
export class AddNewEmployeeModalComponent implements OnInit {
  form: FormGroup
  roles: any
  positions: any;
  UserEMPloyees: IUserEMPloyee
  constructor(
    private toastrService: ToastrService,
    private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private adminService: AdminService
  ) {
    this.form = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null, Validators.required],
      patronymic: [null],
      iin: [null, [Validators.required, Validators.minLength(18)]],
      email: [null, [Validators.required, Validators.email]],
      birthDate: [null, Validators.required],
      phoneNumber: ['+77', [Validators.required, Validators.maxLength(18), Validators.pattern(PhoneRegularExpression)]],
      positionId: [null, Validators.required],
      password: [null, Validators.required],
      confirmPassword: [null, Validators.required],
    })
  }

  ngOnInit(): void {
    this.adminService.sortByPosition().subscribe((data) => (this.roles = data))
    if (this.UserEMPloyees) {
      this.form.patchValue(this.UserEMPloyees)
    }
  }

  onAddEmployee() {
    this.adminService.createUser(this.form.value).subscribe(() => {
      this.adminService.addedUSER.next(true)
      this.toastrService.success('Сотрудник добавлен')
      this.activeModal.close()
    })
  }

  editEmployee() {
    this.form.addControl('id', new FormControl(this.UserEMPloyees.id))
    this.adminService.updateUser(this.form.value).subscribe(() => {
      this.adminService.addedUSER.next(true)
      this.toastrService.success('Обновлен')
      this.activeModal.close()
    })
  }

  changeRole(event) {
    this.adminService.getServiceGroup(event.target.value).subscribe(group => {
      this.positions = group
    })
  }

  onConfirm() {
    return passwordMatch(
      this.form.get('password').value,
      this.form.get('confirmPassword').value
    )()
  }
}
