import { Component, OnInit } from '@angular/core'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import { AddNewEmployeeModalComponent } from '../add-new-employee-modal/add-new-employee-modal.component'

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    console
  }

  newEmployee() {
    const ref = this.modalService.open(AddNewEmployeeModalComponent, { size: 'large' })
  }

}
