import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-detail-modal',
  templateUrl: './detail-modal.component.html',
  styleUrls: ['./detail-modal.component.scss'],
})
export class DetailModalComponent implements OnInit {
  IUserEMPloyee: any = null;
  constructor(private adminService: AdminService,
              private toastrService: ToastrService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.adminService.getPersonalDataEmployees(params['id']).subscribe(data => {
        this.IUserEMPloyee = data;
      }, err => {
        this.toastrService.error('Error')
      })
    })
  }

  // getFullName(u?: IUserEMPloyee): string {
  //   return (u?.lastName ?? '') + ' ' + (u?.firstName ?? '')
  // }
}
