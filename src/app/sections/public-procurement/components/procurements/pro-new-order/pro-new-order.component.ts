import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PublicProcurementsService } from 'src/app/services/public-procurements.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-pro-new-order',
  templateUrl: './pro-new-order.component.html',
  styleUrls: ['./pro-new-order.component.scss']
})
export class ProNewOrderComponent implements OnInit {
  form: FormGroup;
  orderList: any[] = [];
  categories: any;
  constructor(
              private publicProcurementService: PublicProcurementsService,
              private router: Router,
              private toastrService: ToastrService,
              private fb: FormBuilder,
              private notificationsService: NotificationsService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
          itemId: [null, Validators.required],
          categoryId: [null, Validators.required],
          name: ['', Validators.required],
          unit: ['', Validators.required],
          quantity: ['', Validators.required],
          comment: ['', Validators.required],
          files: this.fb.array([]),
        });

    this.init();
  }

  init(): void {
    this.publicProcurementService.getListItems().subscribe({
      next: data => {
        this.orderList = data;
      },
      error: err => {
        this.notificationsService.reportError(err);
      }
    })
   }

   chooseItem(event): void {
    this.publicProcurementService.getCategories(event.target.value).subscribe({
      next: data => {
        this.categories = data;
      },
      error: err => {
        this.notificationsService.reportError(err);
      }
    })
   }

   fileChange(event): void {
    if(event.target.files.length > 0) {
      for(const file of event.target.files) {
        (<FormArray>this.form.get('files')).push(new FormControl(file))
      }
    }
   }

  submit(): void {
    this.form.disable();
    this.publicProcurementService.submitNewProcurements(this.form.value).subscribe({
      next: (data) => {
        this.toastrService.success(data?.message);
        this.form.reset();
        this.router.navigate(['/public-procurement/all-list'])
      },
      error: err => {
        this.notificationsService.reportError(err);
      }
    });
  }

}
