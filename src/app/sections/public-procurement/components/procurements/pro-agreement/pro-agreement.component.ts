import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { PublicProcurementsService } from 'src/app/services/public-procurements.service';
import { saveAs } from 'file-saver'
import { PermissionService } from 'src/app/shared/services/permission.service';
import { Permission } from 'src/app/models/user.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DocumnentConclusionComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion/documnent-conclusion.component';
import { DocumnentConclusionFixComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionEditComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-edit/documnent-conclusion-edit.component';
import { DocumnentConclusionHistoryComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-history/documnent-conclusion-history.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-pro-agreement',
  templateUrl: './pro-agreement.component.html',
  styleUrls: ['./pro-agreement.component.scss']
})
export class ProAgreementComponent implements OnInit {
  userList: any;
  taskId: any;
  permission: Permission;

  constructor(private activeRoute: ActivatedRoute,
    public authService: AuthService,
    private modalService: NgbModal,
    private permissionService: PermissionService,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.taskId = params['id'];
      this.init(params['id'])

      this.permissionService.permission(params['id']).subscribe((perm) => {
        this.permission = perm; 
      })

    })
    
  }

  init(id: number) {
    this.employeeService.myConclusions(id).subscribe((data) => {
      this.userList = data;
    })
  }

  repair(id: number): void {
    this.employeeService.repair(id).subscribe(data => {
      this.init(this.taskId)
    })
  }


  save(item) {
    this.employeeService.downloadDocument(item.id).subscribe(data => {
      return saveAs(data, item.title)
     })
  }

  getFullName(item): string {
    return (item.firstName ?? "") +
            ' ' +
            (item.lastName ?? "") +
            ' ' +
            (item?.patronymic ?? '')
  }

  close() {
    this.modalService.dismissAll()
  }

  docRevision(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionComponent)
   ref.componentInstance.documentId = id;

  }

  docRevisionfix(id?: number) {
    const ref = this.modalService.open(DocumnentConclusionFixComponent)
    ref.componentInstance.documentId = id;
 
   }

   applicationRevision(template) {
    const ref = this.modalService.open(DocumnentConclusionEditComponent)
    ref.componentInstance.template = template
   }

  history(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
   ref.componentInstance.documentId = id;
  }

}
