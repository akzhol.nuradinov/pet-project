import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserList } from 'src/app/models/user.model';
import { PublicProcurementsService } from 'src/app/services/public-procurements.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-pro-page',
  templateUrl: './pro-page.component.html',
  styleUrls: ['./pro-page.component.scss']
})
export class ProPageComponent implements OnInit {
  form: FormGroup;
  list: UserList;
  loading = 0;
  constructor(private fb: FormBuilder,
                private router: Router,
                private activeRoute: ActivatedRoute,
                public authService: AuthService,
                private publicProcurementService: PublicProcurementsService,
                private notificationsService: NotificationsService) { }

    ngOnInit(): void {
      this.form = this.fb.group({
        page: 0,
        size: 10
      });
      this.getList();
    }

    getList(): void {
      this.loading++;
      this.publicProcurementService.getPublicProcurements(this.form.value).subscribe({
        next: data => {
          this.loading--;
          this.list = data;
        },
        error: err => {
          this.loading--;
          this.notificationsService.reportError(err);
        }
      });
    }

    onDetail(id) {
      this.router.navigate([id], {relativeTo: this.activeRoute})
    }

    create(): void {
      this.router.navigate(['/public-procurement/new-order'])
    }

    setPage(event) {
      this.form.patchValue({ page: event - 1 })
      this.getList()
    }

    sortAz() { }

}
