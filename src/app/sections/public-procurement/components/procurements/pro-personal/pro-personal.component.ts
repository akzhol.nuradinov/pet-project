import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/models/user.model';
import { EmployeeService } from 'src/app/services/employee.service';
import { PublicProcurementsService } from 'src/app/services/public-procurements.service';
import { PermissionService } from 'src/app/shared/services/permission.service';

@Component({
  selector: 'app-pro-personal',
  templateUrl: './pro-personal.component.html',
  styleUrls: ['./pro-personal.component.scss']
})
export class ProPersonalComponent implements OnInit {
  switchCase: string = 'personality'
  id: number;
  data: any;
  progress: any;
  permission: Permission;
  form: FormGroup;
  applicationId: number = 0;
  constructor(private activeRoute: ActivatedRoute,
    private permissionService: PermissionService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastrService: ToastrService,
    private publicProcureService: PublicProcurementsService,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {

    this.activeRoute.params.subscribe((params: Params) => {
      this.id = params['id'];

      this.employeeService.applicationProgress(params['id']).subscribe(data => {
          this.progress = data
        });

        this.publicProcureService.getPublicPersonalInfo(params['id']).subscribe(data =>{
          this.applicationId = data?.application.id;
          this.data = data
        })

      this.permissionService.permission(params['id']).subscribe((perm) => {
          this.permission = perm; 
        })
    })

    this.form = this.fb.group({
      applicationId: this.id,
      comment: [null],
      status: [null, Validators.required],
      files: this.fb.array([])
      })

  }
  submit() {
    this.employeeService.submitConclusion(this.form.value).subscribe((data) => {
      this.toastrService.success('Успешно!')
      this.modalService.dismissAll()
    }, err => {
      this.toastrService.error(err?.error?.detail?? 'Oops something went wrong');
    })
  }

  fileChange(event) {
    if(event.target.files.length > 0) {
      for(const file of event.target.files) {
        (<FormArray>this.form.get('files')).push(new FormControl(file))
      }
    }
  }

  close(): void {
    this.modalService.dismissAll();
  }

  openConclusion(modal): void {
    this.modalService.open(modal, {size: 'md'})
  }

}
