import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-pro-history',
  templateUrl: './pro-history.component.html',
  styleUrls: ['./pro-history.component.scss']
})
export class ProHistoryComponent implements OnInit {
  userId: any;
  histories: any;

  constructor(private activeRoute: ActivatedRoute,
    private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.userId = params['id']
      this.employeeService.getHistorList(params['id']).subscribe((data) => {
        this.histories = data;
      })
    })
  }

}
