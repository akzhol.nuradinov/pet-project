import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';
import { PublicProcurementsService } from 'src/app/services/public-procurements.service';
import { saveAs } from 'file-saver'
import { PermissionService } from 'src/app/shared/services/permission.service';
import { Permission } from 'src/app/models/user.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-pro-data',
  templateUrl: './pro-data.component.html',
  styleUrls: ['./pro-data.component.scss']
})
export class ProDataComponent implements OnInit {
  data: any;
  permission: Permission;
  users: any;
  form: FormGroup;
  constructor(private activeRoute: ActivatedRoute,
              private permissionService: PermissionService,
              private modalService: NgbModal,
              private fb: FormBuilder,
              private toastrService: ToastrService,
              private employeeService: EmployeeService,
              private publicProcureService: PublicProcurementsService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {

      this.publicProcureService.getPublicPersonalInfo(params['id']).subscribe(data =>
        this.data = data)
      
      this.permissionService.permission(params['id']).subscribe((perm) => {
          this.permission = perm; 
        })
      this.employeeService.getEmployeesList({applicationId: params['id']}).subscribe(data => {
          this.users = data;
        })

    })

    this.form = this.fb.group({
      receiverId: ['', Validators.required],
      comment: ''
    })
  }

  submit(): void {
    this.employeeService.executerTask({receiverLogin: this.form.value.receiverId}, this.data.application.id).subscribe(() =>{
      this.toastrService.success('Успешно направлено')
      this.modalService.dismissAll();
    }, err => {
      this.toastrService.error('Упс, ошибка')
    })
  }

  openAction(action): void {
    this.modalService.open(action, {size: 'md'})
  }

  fileSave(item) {
    this.employeeService.downloadDocument(item.id).subscribe(data => {
      return saveAs(data, item.title)
     })
  }
  close(): void {
    this.modalService.dismissAll();
  }

  getFullName(item): string {
    return (item.firstName ?? "") +
            ' ' +
            (item.lastName ?? "") +
            ' ' +
            (item?.patronymic ?? '')
  }

}
