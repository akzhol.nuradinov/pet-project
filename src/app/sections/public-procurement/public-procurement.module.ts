import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { DefaultPageComponent } from "./default-page/default-page.component";
import { ProcurementsComponent } from "./components/procurements/procurements.component";
import { ProPageComponent } from "./components/procurements/pro-page/pro-page.component";
import { SharedModule } from "src/app/shared/shared.module";
import { ProPersonalComponent } from "./components/procurements/pro-personal/pro-personal.component";
import { NgbAccordionModule, NgbPaginationModule } from "@ng-bootstrap/ng-bootstrap";
import { ProNewOrderComponent } from "./components/procurements/pro-new-order/pro-new-order.component";
import { ProDataComponent } from "./components/procurements/pro-data/pro-data.component";
import { ProAgreementComponent } from "./components/procurements/pro-agreement/pro-agreement.component";
import { ProHistoryComponent } from "./components/procurements/pro-history/pro-history.component";
const routes: Routes = [
    { path: '', component: DefaultPageComponent, children: [
        { path: 'all-list', component: ProcurementsComponent, children: [
            { path: '', component: ProPageComponent },
            { path: ':id', component: ProPersonalComponent },
        ] },
        { path: 'new-order', component: ProNewOrderComponent}
    ]},
];

@NgModule({
    imports: [
    CommonModule,
    SharedModule,
    NgbPaginationModule,
    NgbAccordionModule,
    RouterModule.forChild(routes)
    ],
    declarations: [
        DefaultPageComponent,
        ProcurementsComponent,
        ProPageComponent,
        ProNewOrderComponent,
        ProDataComponent,
        ProPersonalComponent,
        ProAgreementComponent,
        ProHistoryComponent
    ],
    exports: [RouterModule]
})

export class PublicProcurementModule {}