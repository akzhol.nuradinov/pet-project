import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SharedModule } from "src/app/shared/shared.module";
import { DefaultPageComponent } from "./default-page/default-page.component";
import { CandidatesComponent } from './components/candidates/candidates.component';
import { NgbAccordionModule, NgbCollapseModule, NgbDatepickerModule, NgbPaginationModule, NgbTypeaheadModule } from "@ng-bootstrap/ng-bootstrap";
import { AcceptedCandidatesComponent } from './components/accepted-candidates/accepted-candidates.component';
import { AddCandidateModalComponent } from './components/add-candidate-modal/add-candidate-modal.component';
import { PersonalInfoCandidatesComponent } from './components/candidates/personal-info-candidates/personal-info-candidates.component';
import { PageComponent } from './components/candidates/page/page.component';
import { DataComponent } from './components/candidates/data/data.component';
import { ConclusionComponent } from './components/candidates/conclusion/conclusion.component';
import { HistoryComponent } from './components/candidates/history/history.component';
import { VacacionesComponent } from './components/vacaciones/vacaciones.component';
import { VacationListComponent } from './components/vacaciones/vacation-list/vacation-list.component';
import { PersonalInfoVacationComponent } from './components/vacaciones/personal-info-vacation/personal-info-vacation.component';
import { BusinessTripComponent } from './components/business-trip/business-trip.component';
import { BtListComponent } from './components/business-trip/bt-list/bt-list.component';
import { BtDetailComponent } from './components/business-trip/bt-detail/bt-detail.component';
import { AcPageComponent } from './components/accepted-candidates/ac-page/ac-page.component';
import { AcDataComponent } from './components/accepted-candidates/ac-data/ac-data.component';
import { AcHistoryComponent } from './components/accepted-candidates/ac-history/ac-history.component';
import { AcPersonalComponent } from './components/accepted-candidates/ac-personal/ac-personal.component';
import { AcConclusionComponent } from './components/accepted-candidates/ac-conclusion/ac-conclusion.component';
import { DismissalComponent } from './components/dismissal/dismissal.component';
import { DismissalPageComponent } from './components/dismissal/dismissal-page/dismissal-page.component';
import { DismissalPersonalInfoComponent } from './components/dismissal/dismissal-personal-info/dismissal-personal-info.component';
import { DismissalDataComponent } from './components/dismissal/dismissal-data/dismissal-data.component';
import { DismissalHistoryComponent } from './components/dismissal/dismissal-history/dismissal-history.component';
import { DismissalConclusionComponent } from './components/dismissal/dismissal-conclusion/dismissal-conclusion.component';
import { DismissalByteSheetComponent } from './components/dismissal/dismissal-byte-sheet/dismissal-byte-sheet.component';
import { PromotionComponent } from './components/promotion/promotion.component';
import { PromotionPageComponent } from "./components/promotion/promotion-page/promotion-page.component";
import { PromotionDetailComponent } from './components/promotion/promotion-detail/promotion-detail.component';
import { PromotionDataComponent } from './components/promotion/promotion-data/promotion-data.component';
import { PromotionHistoryComponent } from './components/promotion/promotion-history/promotion-history.component';
import { AbsenceComponent } from './components/absence/absence.component';
import { AbsencePageComponent } from './components/absence/absence-page/absence-page.component';

const routes: Routes = [
    { path: '', component: DefaultPageComponent, children: [
        { path: 'candidates', component: CandidatesComponent, children: [
            { path: '', component: PageComponent},
            { path: ':id', component: PersonalInfoCandidatesComponent}
        ]},
        { path: 'accepted-candidates', component: AcceptedCandidatesComponent, children: [
            { path: '', component: AcPageComponent},
            { path: ':id', component: AcPersonalComponent},
        ]},
        { path: 'vacations', component: VacacionesComponent, children: [
            {path: '', component: VacationListComponent},
            {path: ':id', component: PersonalInfoVacationComponent},
        ]},
        { path: 'business-trip', component: BusinessTripComponent, children: [
            { path: '', component: BtListComponent},
            { path: ':id', component: BtDetailComponent}
        ]},
        { path: 'promotion', component: PromotionComponent, children: [
            { path: '', component: PromotionPageComponent},
            { path: ':id', component: PromotionDetailComponent}
        ]},
        { path: 'absence', component: AbsenceComponent, children: [
            { path: '', component: AbsencePageComponent},
            { path: ':id', component: PromotionDetailComponent}
        ]},
        { path: 'dismissal', component: DismissalComponent, children: [
            { path: '', component: DismissalPageComponent},
            { path: ':id', component: DismissalPersonalInfoComponent}
        ]}
    ]}
]

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        NgbPaginationModule,
        NgbCollapseModule,
        NgbAccordionModule,
        NgbDatepickerModule,
        NgbTypeaheadModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        DefaultPageComponent,
        CandidatesComponent,
        AcceptedCandidatesComponent,
        AddCandidateModalComponent,
        PersonalInfoCandidatesComponent,
        PageComponent,
        DataComponent,
        ConclusionComponent,
        HistoryComponent,
        VacacionesComponent,
        VacationListComponent,
        PersonalInfoVacationComponent,
        BusinessTripComponent,
        BtListComponent,
        BtDetailComponent,
        AcPageComponent,
        AcDataComponent,
        AcHistoryComponent,
        AcPersonalComponent,
        AcConclusionComponent,
        DismissalComponent,
        DismissalPageComponent,
        DismissalPersonalInfoComponent,
        DismissalDataComponent,
        DismissalHistoryComponent,
        DismissalConclusionComponent,
        DismissalByteSheetComponent,
        PromotionComponent,
        PromotionPageComponent,
        PromotionDetailComponent,
        PromotionDataComponent,
        PromotionHistoryComponent,
        AbsenceComponent,
        AbsencePageComponent
    ],
    exports: [RouterModule]
})

export class HumanResourceModule {}