import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserList } from 'src/app/models/user.model';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-ac-page',
  templateUrl: './ac-page.component.html',
  styleUrls: ['./ac-page.component.scss']
})
export class AcPageComponent implements OnInit {
  form: FormGroup;
  candidatesList: UserList;
  column = 'iin';
  direction = 'asc';
  loading: number = 0;
    constructor(private fb: FormBuilder,
                private router: Router,
                private activeRouter: ActivatedRoute,
                private humanResources: HumanResourcesService,
                private notificationService: NotificationsService) { }

    ngOnInit(): void {
      this.form = this.fb.group({
        page: 0,
        size: 10,
        column: this.column,
        direction: this.direction,
        approved :true
      });
      this.getCandidates();
    }

    getCandidates(): void {
      this.loading++;
      this.humanResources.getCandidates(this.form.value).subscribe({
          next: data => {
          this.candidatesList = data;
          this.loading--;
        },
        error: (err) => {
          this.loading--;
          this.notificationService.reportError(err);
        }
      });
    }

    onDetail(id) {
      this.router.navigate([id], {relativeTo: this.activeRouter})
    }

    newCandidate(): void {

    }

    setPage(event) {
      this.form.patchValue({ page: event - 1 })
      this.getCandidates()
    }

    sortAz(key: string) {
      if (this.column == key) {
        if (this.direction === 'asc') {
          this.direction = 'desc'
        } else {
          this.direction = 'asc'
        }
      } else {
        this.direction = 'asc'
      }
      this.column = key
      this.form.patchValue({
        column: this.column,
        direction: this.direction
      })
      this.getCandidates()
     }

     filterDate(event) {
      if (event.target.value) {
        if (!!this.form.value.date) {
     this.form.patchValue({ date: event.target.value })
   } else {
     this.form.addControl('date', new FormControl(event.target.value))
   }
   } else {
     this.form.removeControl('date');
   }

   this.getCandidates();
     }

}
