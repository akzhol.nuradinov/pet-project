import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-absence-page',
  templateUrl: './absence-page.component.html',
  styleUrls: ['./absence-page.component.scss']
})
export class AbsencePageComponent implements OnInit {
  form: FormGroup;
  formCreator: FormGroup;
  public isCollapsed = false;
  candidatesList: UserList;
  column = 'iin';
  direction = 'asc';
  loading: number = 0;
  employees: UserList;


    constructor(private fb: FormBuilder,
                private router: Router,
                private adminService: AdminService,
                private activeRoute: ActivatedRoute,
                private toastrService: ToastrService,
                private modalsService: NgbModal,
                public authService: AuthService,
                private humanResources: HumanResourcesService,
                private notificationService: NotificationsService) { }

    ngOnInit(): void {
      this.getEmployees();
      this.form = this.fb.group({
        page: 0,
        size: 10,
        column: this.column,
        direction: this.direction
      });
      this.getCandidates();
      this.init();
    }
    init() {
      this.formCreator = this.fb.group({
        userLogin: ['', Validators.required],
        startDate: ['', Validators.required],
        finishDate: ['', Validators.required],
        reason: [''],
        file: null
      })
    }

    getEmployees(): void {
      this.loading++;
      this.adminService.getEmployees().subscribe({
        next: (data) => {
          this.employees = data;
          this.loading--;
        },
        error: (err) => {
          this.loading--;
          this.notificationService.reportError(err);
        }
      });
    }

    getCandidates(): void {
      this.loading++;
      this.humanResources.getAbsence(this.form.value).subscribe({
        next: (response) => {
          this.candidatesList = response;
          this.loading--;
        },
        error: (err) => {
          this.loading--;
          this.notificationService.reportError(err);
        }
      });
    }

    onDetail(id) {
      this.router.navigate([id], {relativeTo: this.activeRoute})
    }

    newCandidate(value): void {
      this.modalsService.open(value, {size: 'md'})
    }

    setPage(event) {
      this.form.patchValue({ page: event - 1 })
      this.getCandidates()
    }

    sortAz(key: string) {
      if (this.column == key) {
        if (this.direction === 'asc') {
          this.direction = 'desc'
        } else {
          this.direction = 'asc'
        }
      } else {
        this.direction = 'asc'
      }
      this.column = key
      this.form.patchValue({
        column: this.column,
        direction: this.direction
      })
      this.getCandidates()
     }

     fileChange(event) {
      if(event.target.files && event.target.files.length > 0) {
        console.log(event.target.files)
        this.formCreator.patchValue({ file: event.target.files[0] })
      }
     }

     filterDate(event) {
      if (event.target.value) {
           if (!!this.form.value.date) {
        this.form.patchValue({ date: event.target.value })
      } else {
        this.form.addControl('date', new FormControl(event.target.value))
      }
      } else {
        this.form.removeControl('date');
      }

      this.getCandidates();
     }

     submit() {
      this.loading++;
      this.humanResources.createAbsence(cleanNullProps(this.formCreator.value)).subscribe({
          next: data => {
              this.toastrService.success(data?.message.detail)
              this.modalsService.dismissAll();
              this.getCandidates();
              this.form.reset();
              this.loading--;
          },
          error: (err) => {
            this.loading--;
            this.notificationService.reportError(err);
          }
        });
     }

     getFullName(item): string {
      return (item.firstName ?? "") +
              ' ' +
              (item.lastName ?? "") +
              ' ' +
              (item?.patronymic ?? '')
    }

    close() {
      this.modalsService.dismissAll()
    }
}
