import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-promotion-history',
  templateUrl: './promotion-history.component.html',
  styleUrls: ['./promotion-history.component.scss']
})
export class PromotionHistoryComponent implements OnInit {
  userId: number;
  histories: any

  constructor(private activeRoute: ActivatedRoute,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.userId = params['id']
      this.employeeService.getHistorList(params['id']).subscribe((data) => {
        this.histories = data;
      })
    })
  }

  getFullName(user): string {
    return (
      (user?.receiver?.lastName ?? '') +
      (user?.receiver?.firstName ?? '') +
      (user?.receiver?.patronymic ?? '')
    );
  }
}
