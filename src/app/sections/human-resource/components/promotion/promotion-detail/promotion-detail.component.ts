import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Permission } from 'src/app/models/user.model';
import { HumanResourcesService } from 'src/app/services/human-resources.service';

@Component({
  selector: 'app-promotion-detail',
  templateUrl: './promotion-detail.component.html',
  styleUrls: ['./promotion-detail.component.scss']
})
export class PromotionDetailComponent implements OnInit {
  candidateData: any;
  switchCase: string = 'personality'
  form: FormGroup;
  candidateId: number;
  constructor(private humanService: HumanResourcesService,
              private fb: FormBuilder,
              private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.candidateId = params['id'];
      this.humanService.getPersonalDataPromotion(params['id']).subscribe(data => {
        this.candidateData = data;
      })
  
    })
    this.form = this.fb.group({
      applicationId: this.candidateId,
      documents: this.fb.array([]),
      comment: [null],
      status: [null, Validators.required],
    })
  }

}
