import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/models/user.model';
import { EmployeeService } from 'src/app/services/employee.service';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { PermissionService } from 'src/app/shared/services/permission.service';

@Component({
  selector: 'app-personal-info-vacation',
  templateUrl: './personal-info-vacation.component.html',
  styleUrls: ['./personal-info-vacation.component.scss']
})
export class PersonalInfoVacationComponent implements OnInit {
  candidateId: any;
  candidateData: any;
  permission: Permission;
  users: any;
  progress: any;


  formDirect: FormGroup;
  form: FormGroup;

  constructor(private activeRoute: ActivatedRoute,
              private fb: FormBuilder,
              private employeeService: EmployeeService,
              private toastrService: ToastrService,
              private modalService: NgbModal,
              private permissionService: PermissionService,
              private humanService: HumanResourcesService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.candidateId = params['id']
      this.humanService.getPersonalDataVacation(params['id']).subscribe((data) => {
        this.candidateData = data;
      })
      this.permissionService.permission(params['id']).subscribe(permission => {
        this.permission = permission;
      });
      this.employeeService.getEmployeesList({applicationId: this.candidateId}).subscribe(data => {
        this.users = data;
      })  
      this.employeeService.applicationProgress(this.candidateId).subscribe(data => {
        this.progress = data
      }); 
    })
    this.form = this.fb.group({
      applicationId: this.candidateId,
      comment: [null],
      status: [null, Validators.required],
    })

    this.formDirect = this.fb.group({
      receiverId: ['', Validators.required],
      comment: ''
    })
    
  }

  openConclusion(modal): void {
    this.modalService.open(modal, {size: 'md'})
  }

  openAction(action): void {
    this.modalService.open(action, {size: 'md'})
  }

  accept(): void {
    this.employeeService.acceptApplication({status: 10, id: this.candidateData?.applicationId}).subscribe(() => {
      this.toastrService.success('Успешно')
    }, err => {
      this.toastrService.error('Упс, ошибка')
    }
    )
  }

  cancel(): void {
    this.employeeService.acceptApplication({status: 11, id: this.candidateData?.applicationId}).subscribe(() => {
      this.toastrService.success('Успешно')
    }, err => {
      this.toastrService.error('Упс, ошибка')
    }
    )
  }
  getFullName(user): string {
    return (user?.firstName + ' ' + user?.lastName)
    }

  submit() {
      this.employeeService.submitConclusion(this.form.value).subscribe((data) => {
        this.toastrService.success('Успешно!')
        this.modalService.dismissAll()
      }, err => {
        this.toastrService.error(err?.error?.detail?? 'Oops something went wrong');
      })
    }
  
  redirect(): void {
      this.employeeService.executerTask({receiverLogin: this.formDirect.value.receiverId}, this.candidateData?.applicationId).subscribe(() =>{
        this.toastrService.success('Успешно направлено')
        this.modalService.dismissAll();
      }, err => {
        this.toastrService.error('Упс, ошибка')
      })
    }


}
