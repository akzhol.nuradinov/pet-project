import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';

@Component({
  selector: 'app-vacation-list',
  templateUrl: './vacation-list.component.html',
  styleUrls: ['./vacation-list.component.scss']
})
export class VacationListComponent implements OnInit {
  filterform: FormGroup;
  form: FormGroup;
  employees: UserList;
  vacationList: UserList;
  column = 'iin';
  direction = 'asc';
  loading: number = 0;
  constructor(public authService: AuthService,
              private router: Router,
              private toastrService: ToastrService,
              private activeRoute: ActivatedRoute,
              private adminService: AdminService,
              private modalService: NgbModal,
              private humanResourceService: HumanResourcesService,
              private fb: FormBuilder,
              private notificationService: NotificationsService) { }

  ngOnInit(): void {
    this.filterform = this.fb.group({
      page: 0,
      size: 10,
      column: this.column,
      direction: this.direction
    });
    this.getList();
    this.getEmployees();
    this.init()
  }

  init(): void {
    this.form = this.fb.group({
      userLogin: null,
      startDate: null,
      finishDate: null,
      period: null,
    });
  }

  getEmployees(): void {
    this.loading++;
    this.adminService.getEmployees().subscribe({
      next: (data) => {
        this.employees = data;
        this.loading--;
      },
      error: (err) => {
        this.loading--;
        this.notificationService.reportError(err);
      }
    });
  }

  getList(): void {
    this.loading++;
    this.humanResourceService.getVacations().subscribe({
      next: (response) => {
        this.vacationList = response;
        this.loading--;
      },
      error: (err) => {
        this.loading--;
        this.notificationService.reportError(err);
      }
    });
  }

  newVacation(vac): void {
    this.modalService.open(vac)
   }

   onDetail(id: number): void {
    this.router.navigate([id], { relativeTo: this.activeRoute})
   }

   sortAz(key: string): void {
    if (this.column == key) {
      if (this.direction === 'asc') {
        this.direction = 'desc'
      } else {
        this.direction = 'asc'
      }
    } else {
      this.direction = 'asc'
    }
    this.column = key
    this.form.patchValue({
      column: this.column,
      direction: this.direction
    })
    this.getList()
  }

  getFullName(user): string {
    return (user?.firstName + ' ' + user?.lastName)
  }

  submit(): void {
    this.form.disable();
    this.humanResourceService.createVacation(this.form.value).subscribe({
      next: data => {
        this.form.enable();
        this.toastrService.success('Успешно');
        this.modalService.dismissAll();
        this.getList();
      },
      error: err => {
        this.form.enable();
        this.notificationService.reportError(err);
      }
    })
  }

  setPage(event) {
    this.filterform.patchValue({ page: event - 1 })
    this.getList()
  }

  filterDate(event) {
    if (event.target.value) {
          if (!!this.form.value.date) {
      this.form.patchValue({ date: event.target.value })
    } else {
      this.form.addControl('date', new FormControl(event.target.value))
    }
    } else {
      this.form.removeControl('date');
    }
    this.getList();
   }
}
