import { HttpEvent, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-add-candidate-modal',
  templateUrl: './add-candidate-modal.component.html',
  styleUrls: ['./add-candidate-modal.component.scss']
})
export class AddCandidateModalComponent implements OnInit {
form: FormGroup;
public isCollapsed = false;
  constructor(private fb: FormBuilder,
              private activeModal: NgbActiveModal,
              private toastrService: ToastrService,
              private humanResourceService: HumanResourcesService,
              private notificationService: NotificationsService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      firstName: [null, Validators.required],
      lastName: [null , Validators.required],
      patronymic: ["", []],
      iin: [null , [Validators.required, Validators.minLength(12)]],
      email: [null , [Validators.required, Validators.email]],
      phoneNumber: ['+77' , [Validators.required, Validators.minLength(13)]],
      address: [null , Validators.required],
      militaryService: [null , Validators.required],
      identificationIssued: [null , Validators.required],
      identity: [null , [Validators.required, Validators.minLength(9)]],
      residenceAddress: [null , Validators.required],
      education: this.fb.array([ this.fb.group({
        educationalLevel: null,
        institution: null,
        qualification: null,
        diplomaDate: null,
        diploma: null
      })]),
      familyStatus: [null , Validators.required],
      resume: [null , Validators.required],
      birthdate: [null , Validators.required],
      spouseName: [null],
      spouseBirthdate: [null],
      children: this.fb.array([])
    })
  }

  submit() {
    this.form.disable();
    this.humanResourceService.submit(cleanNullProps(this.form.value)).subscribe({
      next: () => {
        this.form.enable();
        this.toastrService.success('Сохранено успешно')
        this.activeModal.close();
        this.humanResourceService.candidateAdded.next(true)
      },
      error: err => {
        this.form.enable();
        this.notificationService.reportError(err);
      }
    })
  }

  fileChange(event) {
    if(event.target.files) {
      for(const file of event.target.files) {
        this.form.get('resume').patchValue(new FormControl(file))
      }
    }
  }

  addChild(): void {
    this.children.push(
      this.fb.group({
        name: null,
        birthdate: null,
      })
    )
  }

  addEducation(): void {
    this.education.push(
          this.fb.group({
            educationalLevel: null,
            institution: null,
            qualification: null,
            diplomaDate: null,
            diploma: null
          }))
  }

  remove(event): void {
    if (event === 'children') {
      this.children.removeAt(this.children.length-1)
    } else {
      this.education.removeAt(this.education.length-1)
    }

  }

  get children(): FormArray {
      return this.form.get('children') as FormArray;
    }
  get education(): FormArray {
    return this.form.get('education') as FormArray;
  }

  onChanged(event) {
    if(event.target.value === 'Женат' || event.target.value === 'Замужем') {
      if (!this.form.get('spouseName') && !this.form.get('spouseBirthdate')) {
        this.form.addControl('spouseName', new FormControl());
        this.form.addControl('spouseBirthdate', new FormControl());
      }
      this.isCollapsed = true;
    } else {
      this.form.removeControl('spouseName');
      this.form.removeControl('spouseBirthdate');
      this.children.clear();
      this.isCollapsed = false;
    }
  }
}
