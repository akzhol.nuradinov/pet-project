import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Permission } from 'src/app/models/user.model';
import { EmployeeService } from 'src/app/services/employee.service';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { saveAs } from 'file-saver'


@Component({
  selector: 'app-dismissal-data',
  templateUrl: './dismissal-data.component.html',
  styleUrls: ['./dismissal-data.component.scss']
})
export class DismissalDataComponent implements OnInit {
  candidateData: any;
  users: any;
  candidateId: number;
  permission: Permission;
  form: FormGroup;
  formGroupDocs: FormGroup;
  constructor(private humanService: HumanResourcesService,
              private employeeService: EmployeeService,
              private permissionService: PermissionService,
              private modalService: NgbModal,
              private toastrService: ToastrService,
              private fb:FormBuilder,
              private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.candidateId = params['id']
      this.humanService.getPersonalDataDismissal(params['id']).subscribe(data => {
        this.candidateData = data;
      })
      this.employeeService.getEmployeesList({applicationId: params['id']}).subscribe(data => {
        this.users = data;
      })
      this.permissionService.permission(params['id']).subscribe(permission => {
        this.permission = permission;
      });
    })
    this.form = this.fb.group({
      receiverId: ['', Validators.required],
      comment: ''
    })
    this.formGroupDocs = this.fb.group({
          statement: [null],
          order: [null], 
          worksheet: [null], 
        })
  }

  submit(): void {
    this.employeeService.executerTask({receiverLogin: this.form.value.receiverId}, this.candidateId).subscribe(() =>{
      this.toastrService.success('Успешно направлено')
      this.modalService.dismissAll();
    }, err => {
      this.toastrService.error('Упс, ошибка')
    })
  }

  getFullName(u?: any): string {
    return (u?.lastName ?? '') + ' ' + (u?.firstName ?? '')
  }

  openAction(action): void {
    this.modalService.open(action, {size: 'md'})
  }

  accept(): void {
    this.employeeService.acceptApplication({status: 10, id: this.candidateId}).subscribe(() => {
      this.toastrService.success('Успешно')
    }, err => {
      this.toastrService.error('Упс, ошибка')
    }
    )
  }

  addDocs(event) {
    this.modalService.open(event) 
  }

  cancel(): void {
    this.employeeService.acceptApplication({status: 12, id: this.candidateId}).subscribe(() => {
      this.toastrService.success('Успешно')
    }, err => {
      this.toastrService.error('Упс, ошибка')
    }
    )
  }


  save(docs, name): void {
    this.employeeService.downloadDocument(docs.id).subscribe(data => {
      return saveAs(data, name)
     })
  }

  // file(event, name?): void {
  //   let file = null;
  //   if (event.target.files.length > 0) {
  //     for (let item of event.target.files) {
  //       file = item
  //     }
  //   }
  //   this.formGroupDocs.get([name]).patchValue(file)
  // }

  sendDocs(): void {
    console.log(this.formGroupDocs.value)
    this.employeeService.sendDocuments(this.formGroupDocs.value, this.candidateId).subscribe(() => {})
  }

}
