import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Permission } from 'src/app/models/user.model';
import { DocumnentConclusionEditComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-edit/documnent-conclusion-edit.component';
import { DocumnentConclusionFixComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-fix/documnent-conclusion-fix.component';
import { DocumnentConclusionHistoryComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion-history/documnent-conclusion-history.component';
import { DocumnentConclusionComponent } from 'src/app/sections/employees/credit-manager/components/documnent-conclusion/documnent-conclusion.component';
import { EmployeeService } from 'src/app/services/employee.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { saveAs } from 'file-saver';


@Component({
  selector: 'app-dismissal-conclusion',
  templateUrl: './dismissal-conclusion.component.html',
  styleUrls: ['./dismissal-conclusion.component.scss']
})
export class DismissalConclusionComponent implements OnInit {
  userList: any
  taskId: number;
  conclusionId: number;
  permission: Permission;
  form: FormGroup;

  constructor(
    private activeRoute: ActivatedRoute,
    public authService: AuthService,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private permissionService: PermissionService,
    private employeeService: EmployeeService
  ) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.taskId = params['id'];
      this.init(params['id'])
    })

    this.permissionService.permission(this.taskId).subscribe((perm) => {
      this.permission = perm; 
    })

    this.form = this.fb.group({
      status: null,
      comment: null
    })

  }

  init(id: number) {
    this.employeeService.myConclusions(id).subscribe((data) => {
      this.userList = data;
    })
  }

  save(blob, name: string) {
    this.employeeService.downloadDocument(blob.id).subscribe(data => {
      return saveAs(data, name)
     })
  }
  repair(id: number): void {
    this.employeeService.repair(id).subscribe(data => {
      this.init(this.taskId)
    })
  }

  getFullName(user): string {
    return (
      (user?.sender?.lastName ?? '') + ' ' +
      (user?.sender?.firstName ?? '') + ' ' +
      (user?.sender?.patronymic ?? '')
    )
  }

  close() {
    this.modalService.dismissAll()
  }

  docRevision(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionComponent)
   ref.componentInstance.documentId = id;

  }

  docRevisionfix(id?: number) {
    const ref = this.modalService.open(DocumnentConclusionFixComponent)
    ref.componentInstance.documentId = id;
 
   }

   applicationRevision(template) {
    const ref = this.modalService.open(DocumnentConclusionEditComponent)
    ref.componentInstance.template = template
   }

  history(id?: number) {
   const ref = this.modalService.open(DocumnentConclusionHistoryComponent)
   ref.componentInstance.documentId = id;
  }
}
