import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { UserList } from 'src/app/models/user.model';
import { AdminService } from 'src/app/services/admin.service';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { NotificationsService } from 'src/app/shared/services/notifications.service';
import { cleanNullProps } from 'src/app/utils';

@Component({
  selector: 'app-dismissal-page',
  templateUrl: './dismissal-page.component.html',
  styleUrls: ['./dismissal-page.component.scss']
})
export class DismissalPageComponent implements OnInit {
  form: FormGroup;
  formCreator: FormGroup;
  public isCollapsed = false;
  candidatesList: UserList;
  column = 'iin';
  direction = 'asc';
  loading: number = 0;
  employees: UserList;

    constructor(private fb: FormBuilder,
                private router: Router,
                private adminService: AdminService,
                private activeRoute: ActivatedRoute,
                private toastrService: ToastrService,
                private modalsService: NgbModal,
                public authService: AuthService,
                private humanResources: HumanResourcesService,
                private notificationService: NotificationsService) { }

    ngOnInit(): void {
      this.getEmployees();
      this.form = this.fb.group({
        page: 0,
        size: 10,
        column: this.column,
        direction: this.direction
      });
      this.getCandidates();
      this.init();
    }
    init() {
      this.formCreator = this.fb.group({
        userLogin: ['', Validators.required],
        reason: ['', Validators.required],
        dismissalDate: ['', Validators.required],
        vacationBalance: ['', Validators.required],
        discription: [''],
        file: ['']
      })
    }

    getEmployees(): void {
      this.loading++;
      this.adminService.getEmployees().subscribe({
        next: (data) => {
          this.employees = data;
          this.loading--;
        },
        error: (err) => {
          this.loading--;
          this.notificationService.reportError(err);
        }
      });
    }

    getCandidates(): void {
      this.loading++;
      this.humanResources.getDismissal(this.form.value).subscribe({
        next: (response) => {
          this.candidatesList = response;
          this.loading--;
        },
        error: (err) => {
          this.loading--;
          this.notificationService.reportError(err);
        }
      });
    }

    onDetail(id) {
      this.router.navigate([id], {relativeTo: this.activeRoute})
    }

    newCandidate(value): void {
      this.modalsService.open(value, {size: 'md'})
    }

    setPage(event) {
      this.form.patchValue({ page: event - 1 })
      this.getCandidates()
    }

    sortAz(key: string) {
      if (this.column == key) {
        if (this.direction === 'asc') {
          this.direction = 'desc'
        } else {
          this.direction = 'asc'
        }
      } else {
        this.direction = 'asc'
      }
      this.column = key
      this.form.patchValue({
        column: this.column,
        direction: this.direction
      })
      this.getCandidates()
     }

     fileChange(event) {
      if(event.target.files && event.target.files.length > 0) {
        this.formCreator.patchValue({ file: event.target.files[0] })
      }
     }

     filterDate(event) {
      if (event.target.value) {
           if (!!this.form.value.date) {
        this.form.patchValue({ date: event.target.value })
      } else {
        this.form.addControl('date', new FormControl(event.target.value))
      }
      } else {
        this.form.removeControl('date');
      }

      this.getCandidates();
     }

     submit() {
      this.formCreator.disable();
      this.humanResources.createDismissal(cleanNullProps(this.formCreator.value)).subscribe({
        next: data => {
          this.toastrService.success(data?.message)
          this.modalsService.dismissAll();
          this.getCandidates();
          this.form.reset();
          this.formCreator.enable();
        },
        error: err => {
          this.formCreator.enable();
          this.notificationService.reportError(err);
        }
     });
    }

    getFullName(item): string {
      return (item.firstName ?? "") +
              ' ' +
              (item.lastName ?? "") +
              ' ' +
              (item?.patronymic ?? '')
    }

    close() {
      this.modalsService.dismissAll()
    }
}
