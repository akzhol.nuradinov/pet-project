import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from 'src/app/services/employee.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  userId: number;
  histories: any

  constructor(private activeRoute: ActivatedRoute,
              private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe((params: Params) => {
      this.userId = params['id']
      this.employeeService.getHistorList(params['id']).subscribe((data) => {
        this.histories = data;
      })
    })
  }

  getFullName(user): string {
    return (
      (user?.receiver?.lastName ?? '') +
      (user?.receiver?.firstName ?? '') +
      (user?.receiver?.patronymic ?? '')
    );
  }
}
