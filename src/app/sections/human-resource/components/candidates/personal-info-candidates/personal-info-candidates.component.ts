import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HumanResourcesService } from 'src/app/services/human-resources.service';
import { saveAs } from 'file-saver';
import { EmployeeService } from 'src/app/services/employee.service';
import { ToastrService } from 'ngx-toastr';
import { PermissionService } from 'src/app/shared/services/permission.service';
import { Permission } from 'src/app/models/user.model';
import { cleanNullProps } from 'src/app/utils';


@Component({
  selector: 'app-personal-info-candidates',
  templateUrl: './personal-info-candidates.component.html',
  styleUrls: ['./personal-info-candidates.component.scss']
})
export class PersonalInfoCandidatesComponent implements OnInit {
  candidateData: any;
  switchCase: string = 'personality'
  files = [];
  form: FormGroup;
  candidateId: number;
  permission: Permission;
  progress: any;
  constructor(private humanService: HumanResourcesService,
              private employeeService: EmployeeService,
              private permissionService: PermissionService,
              private modalService: NgbModal,
              private toastrService: ToastrService,
              private fb: FormBuilder,
              private activeRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.candidateId = params['id'];
      this.humanService.getPersonalDataCandidates(params['id']).subscribe(data => {
        this.candidateData = data;
      })
      this.permissionService.permission(params['id']).subscribe(permission => {
        this.permission = permission;
      });
      
        this.employeeService.applicationProgress(params['id']).subscribe(data => {
          this.progress = data
        });
  
    })
    this.form = this.fb.group({
      applicationId: this.candidateId,
      documents: this.fb.array([]),
      comment: [null],
      status: [null, Validators.required],
    })
  }

  get docFiles() {
    return this.form.get('documents') as FormArray;
  }

  openConclusion(modal): void {
    this.modalService.open(modal, {size: 'md'})
  }

  fileOnChange(event): void {
    this.files = []
    if (event.target?.files.length > 0) {
      for (let fileData of event.target?.files) {
        this.files.push(fileData?.name)
        this.docFiles.push(new  FormControl(fileData)) 
      }
      
    }
  }

  submit() {
    this.employeeService.submitConclusion(cleanNullProps(this.form.value)).subscribe((data) => {
      this.toastrService.success('Успешно!')
      this.modalService.dismissAll()
    }, err => {
      this.toastrService.error(err?.error?.detail?? 'Oops something went wrong');
    })
  }

  onFormConclusion() {
    this.employeeService.onForm().subscribe((blob) => {
      saveAs(blob, 'file')
    })
  }

  close(): void {
    this.modalService.dismissAll();
  }

}


